using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;

namespace MoviesManagement.Inbound.Application.Domain
{
    public class Actor : IActor
    {
        private readonly ILogger<Actor> _logger;

        private readonly IOperationResult _result;

        private readonly ActorDto _actorDto;
        private readonly IEnumerable<IMovie> _movies;

        private readonly IActorRepository _actorRepository;

        public Actor(ILogger<Actor> logger,
            IOperationResult result,
            ActorDto actorDto,
            IEnumerable<IMovie> movies,
            IActorRepository actorRepository)
        {
            _logger = logger;
            _result = result;
            _actorDto = actorDto;
            _movies = movies;
            _actorRepository = actorRepository;
        }

        public bool Exists()
        {
            return _actorDto != null;
        }

        public bool IsValid()
        {
            return 
                !string.IsNullOrEmpty(_actorDto.FirstName) &&
                !string.IsNullOrEmpty(_actorDto.LastName) &&
                _movies.All(movie => movie.Exists());
        }

        public bool HasAlready(IEnumerable<string> actorIds)
        {
            return actorIds.Contains(_actorDto.Id);
        }

        public void NotifyThatAlreadyExists(string movie)
        {
            _logger.LogWarning($"Actor {_actorDto} has already exists in movie {movie}");
            _result.NotifyAboutNotUpdating();
        }

        public void NotifyThatNotExists()
        {
            _logger.LogWarning($"Actor {_actorDto} does not exists");
            _result.NotifyAboutNotUpdating();
        }

        public async Task AppendTo(IMovie movie)
        {
            await movie.Append(_actorDto.Id);
            _result.NotifyAboutUpdate();
        }

        public async Task Insert()
        {
            await _actorRepository.Insert(_actorDto);
            _result.NotifyAboutCreated(_actorDto);
        }

        public void NotifyAboutValidationFailed()
        {
            _logger.LogWarning(ThatOccuredOneOfTheFollowingErrors());
            _result.NotifyAboutValidationFailed();
        }

        public async Task Update()
        {
            await _actorRepository.Update(_actorDto);
            _result.NotifyAboutUpdate(_actorDto);
        }

        public bool HasAlready(IMovie movie)
        {
            return movie.HasAlready(FilmographyIds());
        }

        public void NotifyThatAlreadyExists(IMovie movie)
        {
            movie.NotifyThatAlreadyExists(_actorDto.ToString());
        }

        public async Task Append(string movieId)
        {
            await _actorRepository.AddMovieToActor(movieId, _actorDto.Id);
        }
        
        private IEnumerable<string> FilmographyIds()
        {
            return _actorDto.Filmography
                .Select(filmography =>
                    filmography.Id);
        }

        private string ThatOccuredOneOfTheFollowingErrors()
        {
            return $"One of the following errors occured during validation movie {_actorDto}:\r\n" +
                   "1. Actor does not has existing movie" +
                   "2. Actor has empty first name\r\n" +
                   "3. Actor has empty last name\r\n";
        }
    }
}