using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;

namespace MoviesManagement.Inbound.Application.Domain
{   
    public class Movie : IMovie
    {
        private readonly ILogger<Movie> _logger;
        private readonly MovieDto _movieDto;
        private readonly IOperationResult _result;
        private readonly IMovieRepository _movieRepository;
        private readonly IEnumerable<IActor> _actors;

        public Movie(ILogger<Movie> logger,
            IOperationResult result,
            MovieDto movieDto,
            IEnumerable<IActor> actors,
            IMovieRepository movieRepository)
        {
            _logger = logger;
            _movieDto = movieDto;
            _result = result;
            _movieRepository = movieRepository;
            _actors = actors;
        }

        public bool HasAlready(IActor actor)
        {
            return actor.HasAlready(StarringActorIds());
        }

        public void NotifyThatAlreadyExists(IActor actor)
        {
            actor.NotifyThatAlreadyExists(_movieDto.ToString());
        }

        public bool Exists()
        {
            return _movieDto != null;
        }

        public void NotifyThatNotExists()
        {
            _logger.LogWarning($"Movie {_movieDto} does not exists");
            _result.NotifyAboutNotUpdating();
        }

        public bool IsValid()
        {
            return MovieYearIsLessOrEqualToCurrentYear() && ActorsExist();
        }

        public void NotifyAboutValidationFailed()
        {
            _logger.LogWarning(ThatOccuredOneOfTheFollowingErrors());
            _result.NotifyAboutValidationFailed();
        }

        public async Task Insert()
        {
            await _movieRepository.Insert(_movieDto);
            _result.NotifyAboutCreated(_movieDto);
        }

        public async Task Update()
        {
            await _movieRepository.Update(_movieDto);
            _result.NotifyAboutUpdate(_movieDto);
        }

        public bool HasAlready(IEnumerable<string> movieIds)
        {
            return movieIds.Contains(_movieDto.Id);
        }

        public async Task Append(string actorId)
        {
            await _movieRepository.AddActorToMovie(actorId, _movieDto.Id);
        }

        private IEnumerable<string> StarringActorIds()
        {
            return _movieDto.StarringActors
                .Select(starringActor =>
                    starringActor.Id);
        }

        private bool MovieYearIsLessOrEqualToCurrentYear()
        {
            return _movieDto.Year <= DateTime.UtcNow.Year;
        }

        private bool ActorsExist()
        {
            return _actors.Any() && _actors.All(actor => actor.Exists());
        }

        public async Task AppendTo(IActor actor)
        {
            await actor.Append(_movieDto.Id);
            _result.NotifyAboutUpdate();
        }

        public void NotifyThatAlreadyExists(string actor)
        {
            _logger.LogWarning($"Movie {_movieDto} has already exists in actor {actor}");
            _result.NotifyAboutNotUpdating();
        }

        private string ThatOccuredOneOfTheFollowingErrors()
        {
            return $"One of the following errors occured during validation movie {_movieDto}:\r\n" +
                   "1. Movie does not has any actor\r\n" +
                   "2. Movie has at least one invalid actor\r\n" +
                   "3. Movie has year greater than current year";
        }
    }
}