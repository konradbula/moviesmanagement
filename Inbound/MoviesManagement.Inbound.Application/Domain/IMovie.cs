using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesManagement.Inbound.Application.Domain
{
    public interface IMovie
    {
        bool HasAlready(IActor actor);
        void NotifyThatAlreadyExists(IActor actor);
        Task Append(string actorId);
        bool Exists();
        void NotifyThatNotExists();
        bool IsValid();
        void NotifyAboutValidationFailed();
        Task Insert();
        Task Update();
        bool HasAlready(IEnumerable<string> movieIds);
        Task AppendTo(IActor actor);
        void NotifyThatAlreadyExists(string actor);
    }
}