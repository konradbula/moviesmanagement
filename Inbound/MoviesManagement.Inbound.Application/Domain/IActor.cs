using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesManagement.Inbound.Application.Domain
{
    public interface IActor
    {
        bool HasAlready(IEnumerable<string> actorIds);
        void NotifyThatAlreadyExists(string movie);
        Task AppendTo(IMovie movie);
        bool Exists();
        void NotifyThatNotExists();
        Task Insert();
        bool IsValid();
        void NotifyAboutValidationFailed();
        Task Update();
        Task Append(string movieId);
        bool HasAlready(IMovie movie);
        void NotifyThatAlreadyExists(IMovie movie);
    }
}