﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Application.Requests.Creates;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Application.Requests.Removes;
using MoviesManagement.Inbound.Application.Requests.Updates;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests
{
    public class InboundUpdateRequestFactory : IUpdateRequestFactory
    {
        private const string AddActorRequestName = "AddActorRequest";
        private const string AddMovieRequestName = "AddMovieRequest";
        private const string UpdateActorRequestName = "UpdateActorRequest";
        private const string UpdateMovieRequestName = "UpdateMovieRequest";
        private const string RemoveActorRequestName = "RemoveActorRequest";
        private const string RemoveMovieRequestName = "RemoveMovieRequest";
        private const string LinkExistingActorToExistingMovieRequestName =
            "LinkExistingActorToExistingMovieRequest";
        private const string LinkExistingMovieToExistingActorRequestName =
            "LinkExistingMovieToExistingActorRequest";

        private readonly ILoggerFactory _loggerFactory;
        private readonly IRequestCounter _requestCounter;
        private readonly IActorRepository _actorsRepository;
        private readonly IMovieRepository _movieRepository;

        public InboundUpdateRequestFactory(ILoggerFactory loggerFactory,
            IRequestCounter requestCounter,
            IActorRepository actorsRepository,
            IMovieRepository movieRepository)
        {
            _loggerFactory = loggerFactory;
            _requestCounter = requestCounter;
            _actorsRepository = actorsRepository;
            _movieRepository = movieRepository;
        }

        public IInboundRequest CreateAddActor(ActorDto actorDto, IOperationResult result)
        {
            return new InboundRequestCounter(
                new AddActorRequest(
                    _loggerFactory.CreateLogger<AddActorRequest>(),
                    CreateActorFrom(actorDto, result, CreateMoviesFrom(actorDto.Filmography, result))
                ),
                _requestCounter,
                AddActorRequestName
            );
        }
        
        public IInboundRequest CreateUpdateActor(ActorDto actorDto, IOperationResult result)
        {
            return new InboundRequestCounter(
                new UpdateActorRequest(
                    _loggerFactory.CreateLogger<UpdateActorRequest>(),
                    CreateActorFrom(actorDto, result, CreateMoviesFrom(actorDto.Filmography, result))
                ),
                _requestCounter,
                UpdateActorRequestName
            );
        }

        public IInboundRequest CreateRemoveActor(string actorId, IOperationResult result)
        {
            return new InboundRequestCounter(
                new RemoveActorRequest(
                    _loggerFactory.CreateLogger<RemoveActorRequest>(),
                    actorId,
                    _actorsRepository,
                    _movieRepository,
                    result
                ),
                _requestCounter,
                RemoveActorRequestName
            );
        }

        public IInboundRequest CreateAddMovie(MovieDto movieDto, IOperationResult result)
        {
            return new InboundRequestCounter(
                new AddMovieRequest(
                    _loggerFactory.CreateLogger<AddMovieRequest>(),
                    CreateMovieFrom(movieDto, result, CreateActorsFrom(movieDto.StarringActors, result))
                ),
                _requestCounter,
                AddMovieRequestName
            );
        }
        
        public IInboundRequest CreateUpdateMovie(MovieDto movieDto, IOperationResult result)
        {
            return new InboundRequestCounter(
                new UpdateMovieRequest(
                    _loggerFactory.CreateLogger<UpdateMovieRequest>(),
                    CreateMovieFrom(movieDto, result, CreateActorsFrom(movieDto.StarringActors, result))
                ),
                _requestCounter,
                UpdateMovieRequestName
            );
        }

        public IInboundRequest CreateRemoveMovie(string movieId, IOperationResult result)
        {
            return new InboundRequestCounter(
                new RemoveMovieRequest(
                    _loggerFactory.CreateLogger<RemoveMovieRequest>(),
                    movieId,
                    _actorsRepository,
                    _movieRepository,
                    result
                ),
                _requestCounter,
                RemoveMovieRequestName
            );
        }

        public IInboundRequest CreateLinkExistingActorToExistingMovie(string actorId,
            string movieId,
            IOperationResult result)
        {
            return new InboundRequestCounter(
                new LinkExistingActorToExistingMovieRequest(
                    LinkExistingActorToExistingMovieRequestLogger(),
                    CreateMovieFrom(_movieRepository.GetById(movieId), result, new IActor[0]),
                    CreateActorFrom(_actorsRepository.GetById(actorId), result, new IMovie[0])
                ),
                _requestCounter,
                LinkExistingActorToExistingMovieRequestName
            );
        }

        public IInboundRequest CreateLinkExistingMovieToExistingActor(string movieId, 
            string actorId,
            IOperationResult result)
        {
            return new InboundRequestCounter(
                new LinkExistingMovieToExistingActorRequest(
                    LinkExistingMovieToExistingActorRequestLogger(),
                    CreateMovieFrom(_movieRepository.GetById(movieId), result, new IActor[0]),
                    CreateActorFrom(_actorsRepository.GetById(actorId), result, new IMovie[0])
                ), 
                _requestCounter,
                LinkExistingMovieToExistingActorRequestName
            );
        }

        private IEnumerable<IActor> CreateActorsFrom(IEnumerable<StarringActorDto> starringActorDtos,
            IOperationResult result)
        {
            return starringActorDtos == null
                ? new IActor[0].AsEnumerable()
                : starringActorDtos.Select(starringActorDto =>
                    CreateActorFrom(_actorsRepository.GetById(starringActorDto.Id), result, new IMovie[0]));
        }

        private IEnumerable<IMovie> CreateMoviesFrom(IEnumerable<FilmographyDto> filmographyDtos,
            IOperationResult result)
        {
            return filmographyDtos == null
                ? new IMovie[0].AsEnumerable()
                : filmographyDtos.Select(filmographyDto =>
                    CreateMovieFrom(_movieRepository.GetById(filmographyDto.Id), result, new IActor[0]));
        }

        private Actor CreateActorFrom(ActorDto actorDto, IOperationResult result, IEnumerable<IMovie> movies)
        {
            return new Actor(
                _loggerFactory.CreateLogger<Actor>(),
                result,
                actorDto,
                movies,
                _actorsRepository
            );
        }

        private Movie CreateMovieFrom(MovieDto movieDto, IOperationResult result, IEnumerable<IActor> actors)
        {
            return new Movie(
                _loggerFactory.CreateLogger<Movie>(),
                result,
                movieDto,
                actors, _movieRepository);
        }

        private ILogger<LinkExistingActorToExistingMovieRequest> LinkExistingActorToExistingMovieRequestLogger()
        {
            return _loggerFactory.CreateLogger<LinkExistingActorToExistingMovieRequest>();
        }
        
        private ILogger<LinkExistingMovieToExistingActorRequest> LinkExistingMovieToExistingActorRequestLogger()
        {
            return _loggerFactory.CreateLogger<LinkExistingMovieToExistingActorRequest>();
        }
    }
}