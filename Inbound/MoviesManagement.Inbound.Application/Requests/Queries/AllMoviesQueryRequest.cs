﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Queries
{
    public class AllMoviesQueryRequest : IInboundRequest
    {
        private readonly ILogger<AllMoviesQueryRequest> _logger;
        private readonly IMovieRepository _moviesRepository;
        private readonly IOperationResult _result;

        public AllMoviesQueryRequest(ILogger<AllMoviesQueryRequest> logger,
            IMovieRepository moviesRepository,
            IOperationResult result)
        {
            _logger = logger;
            _moviesRepository = moviesRepository;
            _result = result;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle an all movies query request");
            _result.NotifyAboutQuery(await _moviesRepository.GetAll());
        }
    }
}