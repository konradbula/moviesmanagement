﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Queries
{
    public class MoviesByActorQueryRequest : IInboundRequest
    {
        private readonly ILogger<MoviesByActorQueryRequest> _logger;
        private readonly string _actorId;
        private readonly IMovieRepository _moviesRepository;
        private readonly IOperationResult _result;

        public MoviesByActorQueryRequest(ILogger<MoviesByActorQueryRequest> logger,
            string actorId,
            IMovieRepository moviesRepository,
            IOperationResult result)
        {
            _logger = logger;
            _actorId = actorId;
            _moviesRepository = moviesRepository;
            _result = result;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle a movies by actor query request");
            _result.NotifyAboutQuery(await _moviesRepository.GetByActor(_actorId));
        }
    }
}