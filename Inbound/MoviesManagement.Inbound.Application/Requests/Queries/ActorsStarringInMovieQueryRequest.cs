﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Queries
{
    public class ActorsStarringInMovieQueryRequest : IInboundRequest
    {
        private readonly ILogger<ActorsStarringInMovieQueryRequest> _logger;
        private readonly string _movieId;
        private readonly IActorRepository _actorRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IOperationResult _result;

        public ActorsStarringInMovieQueryRequest(ILogger<ActorsStarringInMovieQueryRequest> logger,
            string movieId,
            IActorRepository actorRepository,
            IMovieRepository movieRepository,
            IOperationResult result)
        {
            _logger = logger;
            _movieId = movieId;
            _actorRepository = actorRepository;
            _movieRepository = movieRepository;
            _result = result;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle an actors starring in a movie query request");
            _result.NotifyAboutQuery(await _actorRepository.GetByIds(await GetActorsStarringInMovie()));
        }

        private async Task<IEnumerable<string>> GetActorsStarringInMovie()
        {
            return await _movieRepository.GetActorsStarringInMovie(_movieId);
        }
    }
}