﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Queries
{
    public class MoviesByYearQueryRequest : IInboundRequest
    {
        private readonly ILogger<MoviesByYearQueryRequest> _logger;
        private readonly int _year;
        private readonly IMovieRepository _moviesRepository;
        private readonly IOperationResult _result;

        public MoviesByYearQueryRequest(ILogger<MoviesByYearQueryRequest> logger,
            int year,
            IMovieRepository moviesRepository,
            IOperationResult result)
        {
            _logger = logger;
            _year = year;
            _moviesRepository = moviesRepository;
            _result = result;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle a movies by year query request");
            _result.NotifyAboutQuery(await _moviesRepository.GetByYear(_year));
        }
    }
}