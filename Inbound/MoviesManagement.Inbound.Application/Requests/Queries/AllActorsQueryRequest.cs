﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Queries
{
    public class AllActorsQueryRequest : IInboundRequest
    {
        private readonly ILogger<AllActorsQueryRequest> _logger;
        private readonly IActorRepository _actorsRepository;
        private readonly IOperationResult _result;

        public AllActorsQueryRequest(ILogger<AllActorsQueryRequest> logger,
            IActorRepository actorsRepository,
            IOperationResult result)
        {
            _logger = logger;
            _actorsRepository = actorsRepository;
            _result = result;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle an all actors query request");
            _result.NotifyAboutQuery(await _actorsRepository.GetAll());
        }
    }
}