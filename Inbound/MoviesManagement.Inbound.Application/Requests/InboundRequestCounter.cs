﻿using System.Threading.Tasks;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests
{
    public class InboundRequestCounter : IInboundRequest
    {
        private readonly IInboundRequest _inboundRequest;
        private readonly IRequestCounter _requestCounter;
        private readonly string _requestName;

        public InboundRequestCounter(IInboundRequest inboundRequest, 
            IRequestCounter requestCounter,
            string requestName)
        {
            _inboundRequest = inboundRequest;
            _requestCounter = requestCounter;
            _requestName = requestName;
        }

        public async Task Handle()
        {
            _requestCounter.Increment(_requestName);
            await _inboundRequest.Handle();
        }
    }
}