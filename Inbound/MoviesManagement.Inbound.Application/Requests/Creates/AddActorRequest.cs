﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Creates
{
    public class AddActorRequest : IInboundRequest
    {
        private readonly ILogger<AddActorRequest> _logger;
        private readonly IActor _actor;

        public AddActorRequest(ILogger<AddActorRequest> logger,
            IActor actor)
        {
            _logger = logger;
            _actor = actor;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle an add actor request");
            
            if (!_actor.IsValid())
            {
                _actor.NotifyAboutValidationFailed();
                return;
            }
            await _actor.Insert();
        }
    }
}