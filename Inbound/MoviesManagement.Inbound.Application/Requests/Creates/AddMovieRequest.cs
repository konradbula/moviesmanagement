﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Creates
{
    public class AddMovieRequest : IInboundRequest
    {
        private readonly ILogger<AddMovieRequest> _logger;
        private readonly IMovie _movie;

        public AddMovieRequest(ILogger<AddMovieRequest> logger,
            IMovie movie)
        {
            _logger = logger;
            _movie = movie;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle an add movie request");
            
            if (!_movie.IsValid())
            {
                _movie.NotifyAboutValidationFailed();
                return;
            }
            await _movie.Insert();
        }
    }
}