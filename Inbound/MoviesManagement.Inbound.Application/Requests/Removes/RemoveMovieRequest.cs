﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Removes
{
    public class RemoveMovieRequest : IInboundRequest
    {
        private readonly ILogger<RemoveMovieRequest> _logger;
        private readonly string _movieId;
        private readonly IActorRepository _actorRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IOperationResult _result;

        public RemoveMovieRequest(ILogger<RemoveMovieRequest> logger,
            string movieId,
            IActorRepository actorRepository,
            IMovieRepository movieRepository,
            IOperationResult result)
        {
            _logger = logger;
            _movieId = movieId;
            _actorRepository = actorRepository;
            _movieRepository = movieRepository;
            _result = result;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle a remove movie request");
            await _actorRepository.RemoveMovieFromFilmography(_movieId);
            await _movieRepository.Remove(_movieId);
            _result.NotifyAboutUpdate();
        }
    }
}