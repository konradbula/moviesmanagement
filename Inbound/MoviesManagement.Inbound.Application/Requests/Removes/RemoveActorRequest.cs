﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Removes
{
    public class RemoveActorRequest : IInboundRequest
    {
        private readonly ILogger<RemoveActorRequest> _logger;
        private readonly string _actorId;
        private readonly IActorRepository _actorRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IOperationResult _result;

        public RemoveActorRequest(ILogger<RemoveActorRequest> logger,
            string actorId,
            IActorRepository actorRepository,
            IMovieRepository movieRepository,
            IOperationResult result)
        {
            _logger = logger;
            _actorId = actorId;
            _actorRepository = actorRepository;
            _movieRepository = movieRepository;
            _result = result;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle a remove actor request");
            await _movieRepository.RemoveActorFromStarringActors(_actorId);
            await _actorRepository.Remove(_actorId);
            _result.NotifyAboutUpdate();
        }
    }
}