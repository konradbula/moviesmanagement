﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Updates
{
    public class UpdateActorRequest : IInboundRequest
    {
        private readonly ILogger<UpdateActorRequest> _logger;
        private readonly IActor _actor;

        public UpdateActorRequest(ILogger<UpdateActorRequest> logger,
            IActor actor)
        {
            _logger = logger;
            _actor = actor;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle an update actor request");
            
            if (!_actor.IsValid())
            {
                _actor.NotifyAboutValidationFailed();
                return;
            }
            await _actor.Update();
        }
    }
}