using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Updates
{
    public class LinkExistingMovieToExistingActorRequest : IInboundRequest
    {
        private readonly ILogger<LinkExistingMovieToExistingActorRequest> _logger;
        private readonly IMovie _movie;
        private readonly IActor _actor;

        public LinkExistingMovieToExistingActorRequest(ILogger<LinkExistingMovieToExistingActorRequest> logger, 
            IMovie movie, 
            IActor actor)
        {
            _logger = logger;
            _movie = movie;
            _actor = actor;
        }
        
        public async Task Handle()
        {
            _logger.LogDebug("Handle an link existing movie to existing actor");
            
            if (!_actor.Exists())
            {
                _actor.NotifyThatNotExists();
                return;
            }

            if (!_movie.Exists())
            {
                _movie.NotifyThatNotExists();
                return;
            }
            
            if (_actor.HasAlready(_movie))
            {
                _actor.NotifyThatAlreadyExists(_movie);
                return;
            }
            
            await _movie.AppendTo(_actor);
        }
    }
}