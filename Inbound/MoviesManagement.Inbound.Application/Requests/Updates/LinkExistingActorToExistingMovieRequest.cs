﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Updates
{
    public class LinkExistingActorToExistingMovieRequest: IInboundRequest
    {
        private readonly ILogger<LinkExistingActorToExistingMovieRequest> _logger;
        private readonly IMovie _movie;
        private readonly IActor _actor;

        public LinkExistingActorToExistingMovieRequest(ILogger<LinkExistingActorToExistingMovieRequest> logger,
            IMovie movie,
            IActor actor)
        {
            _logger = logger;
            _movie = movie;
            _actor = actor;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle an link existing actor to existing movie");
            
            if (!_actor.Exists())
            {
                _actor.NotifyThatNotExists();
                return;
            }

            if (!_movie.Exists())
            {
                _movie.NotifyThatNotExists();
                return;
            }
            
            if (_movie.HasAlready(_actor))
            {
                _movie.NotifyThatAlreadyExists(_actor);
                return;
            }
            
            await _actor.AppendTo(_movie);
        }
    }
}