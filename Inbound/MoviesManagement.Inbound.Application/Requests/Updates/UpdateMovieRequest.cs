﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests.Updates
{
    public class UpdateMovieRequest : IInboundRequest
    {
        private readonly ILogger<UpdateMovieRequest> _logger;
        private readonly IMovie _movie;

        public UpdateMovieRequest(ILogger<UpdateMovieRequest> logger,
            IMovie movie)
        {
            _logger = logger;
            _movie = movie;
        }

        public async Task Handle()
        {
            _logger.LogDebug("Handle an update movie request");
            
            if (!_movie.IsValid())
            {
                _movie.NotifyAboutValidationFailed();
                return;
            }
            await _movie.Update();
        }
    }
}