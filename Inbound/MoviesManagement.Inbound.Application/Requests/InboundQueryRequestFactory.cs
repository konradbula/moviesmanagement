﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Application.Requests.Creates;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Application.Requests.Removes;
using MoviesManagement.Inbound.Application.Requests.Updates;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.Inbound.Application.Requests
{
    public class InboundQueryRequestFactory : IQueryRequestFactory
    {
        private const string AllActorsQueryRequestName = "AllActorsQueryRequest";
        private const string AllMoviesQueryRequestName = "AllMoviesQueryRequest";
        private const string MoviesByActorQueryRequestName = "MoviesByActorQueryRequest";
        private const string MoviesByYearQueryRequestName = "MoviesByYearQueryRequest";
        private const string ActorsStarringInMovieQueryRequestName = "ActorsStarringInMovieQueryRequest";

        private readonly ILoggerFactory _loggerFactory;
        private readonly IRequestCounter _requestCounter;
        private readonly IActorRepository _actorsRepository;
        private readonly IMovieRepository _movieRepository;

        public InboundQueryRequestFactory(ILoggerFactory loggerFactory,
            IRequestCounter requestCounter,
            IActorRepository actorsRepository,
            IMovieRepository movieRepository)
        {
            _loggerFactory = loggerFactory;
            _requestCounter = requestCounter;
            _actorsRepository = actorsRepository;
            _movieRepository = movieRepository;
        }

        public IInboundRequest CreateAllActors(IOperationResult result)
        {
            return new InboundRequestCounter(
                new AllActorsQueryRequest(
                    AllActorsQueryRequestLogger(),
                    _actorsRepository,
                    result
                ),
                _requestCounter,
                AllActorsQueryRequestName
            );
        }

        public IInboundRequest CreateAllMovies(IOperationResult result)
        {
            return new InboundRequestCounter(
                new AllMoviesQueryRequest(
                    AllMoviesQueryRequestLogger(),
                    _movieRepository,
                    result
                ),
                _requestCounter,
                AllMoviesQueryRequestName
            );
        }

        public IInboundRequest CreateMoviesByYear(int year, IOperationResult result)
        {
            return new InboundRequestCounter(
                new MoviesByYearQueryRequest(
                    MoviesByYearQueryRequestLogger(),
                    year,
                    _movieRepository,
                    result
                ),
                _requestCounter,
                MoviesByYearQueryRequestName
            );
        }

        public IInboundRequest CreateMoviesByActor(string actorId, IOperationResult result)
        {
            return new InboundRequestCounter(
                new MoviesByActorQueryRequest(
                    MoviesByActorQueryRequestLogger(),
                    actorId,
                    _movieRepository,
                    result
                ),
                _requestCounter,
                MoviesByActorQueryRequestName
            );
        }

        public IInboundRequest CreateActorsStarringInMovie(string movieId, IOperationResult result)
        {
            return new InboundRequestCounter(
                new ActorsStarringInMovieQueryRequest(
                    ActorsStarringInMovieQueryRequestLogger(),
                    movieId,
                    _actorsRepository,
                    _movieRepository,
                    result
                ),
                _requestCounter,
                ActorsStarringInMovieQueryRequestName
            );
        }

        private ILogger<AllActorsQueryRequest> AllActorsQueryRequestLogger()
        {
            return _loggerFactory.CreateLogger<AllActorsQueryRequest>();
        }

        private ILogger<AllMoviesQueryRequest> AllMoviesQueryRequestLogger()
        {
            return _loggerFactory.CreateLogger<AllMoviesQueryRequest>();
        }

        private ILogger<MoviesByYearQueryRequest> MoviesByYearQueryRequestLogger()
        {
            return _loggerFactory.CreateLogger<MoviesByYearQueryRequest>();
        }

        private ILogger<MoviesByActorQueryRequest> MoviesByActorQueryRequestLogger()
        {
            return _loggerFactory.CreateLogger<MoviesByActorQueryRequest>();
        }

        private ILogger<ActorsStarringInMovieQueryRequest> ActorsStarringInMovieQueryRequestLogger()
        {
            return _loggerFactory.CreateLogger<ActorsStarringInMovieQueryRequest>();
        }
    }
}