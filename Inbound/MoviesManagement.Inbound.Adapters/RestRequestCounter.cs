﻿using Microsoft.ApplicationInsights;
using MoviesManagement.Inbound.Ports;

namespace MoviesManagement.Inbound.Adapters
{
    public class RestRequestCounter : IRequestCounter
    {
        private readonly TelemetryClient _telemetryClient;

        public RestRequestCounter(TelemetryClient telemetryClient)
        {
            _telemetryClient = telemetryClient;
        }

        public void Increment(string requestName)
        {
            _telemetryClient.TrackEvent(requestName);
        }
    }
}