using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;

namespace MoviesManagement.Inbound.Adapters.Repositories
{
    public class MongoDbActorRepository : IActorRepository
    {
        private readonly IMongoCollection<ActorDto> _actorsCollection;

        public MongoDbActorRepository(IMongoDatabase mongoDatabase)
        {
            _actorsCollection = mongoDatabase
                .GetCollection<ActorDto>(typeof(ActorDto).Name);
        }

        public async Task Insert(ActorDto entity)
        {
            await _actorsCollection.InsertOneAsync(entity);
        }

        public async Task<IEnumerable<ActorDto>> GetAll()
        {
            return await (await _actorsCollection.FindAsync(_ => true)).ToListAsync();
        }

        public ActorDto GetById(string id)
        {
            return _actorsCollection
                .FindSync(actor => actor.Id == id)
                .FirstOrDefault();
        }

        public async Task<IEnumerable<ActorDto>> GetByIds(IEnumerable<string> actorIds)
        {
            return await (await _actorsCollection
                .FindAsync(Builders<ActorDto>.Filter.In(actor => actor.Id, actorIds)))
                .ToListAsync();
        }

        public async Task RemoveMovieFromFilmography(string movieId)
        {
            await _actorsCollection.UpdateManyAsync(_ => true,
                Builders<ActorDto>.Update.Pull(actor => actor.Filmography, new FilmographyDto(movieId)));
        }

        public async Task Update(ActorDto actorDto)
        {
            await _actorsCollection.ReplaceOneAsync(actor => actor.Id == actorDto.Id, actorDto);
        }

        public async Task AddMovieToActor(string movieId, string actorId)
        {
            await _actorsCollection.UpdateOneAsync(actor => actor.Id == actorId,
                Builders<ActorDto>.Update.Push(actor => actor.Filmography, new FilmographyDto(movieId)));
        }

        public async Task Remove(string actorId)
        {
            await _actorsCollection.DeleteOneAsync(actor => actor.Id == actorId);
        }
    }
}