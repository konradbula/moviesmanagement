using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;

namespace MoviesManagement.Inbound.Adapters.Repositories
{
    public class MongoDbMovieRepository : IMovieRepository
    {
        private readonly IMongoCollection<MovieDto> _moviesCollection;

        public MongoDbMovieRepository(IMongoDatabase mongoDatabase)
        {
            _moviesCollection = mongoDatabase
                .GetCollection<MovieDto>(typeof(MovieDto).Name);
            SetupIndexes();
        }

        public async Task Insert(MovieDto entity)
        {
            await _moviesCollection.InsertOneAsync(entity);
        }

        public async Task<IEnumerable<MovieDto>> GetAll()
        {
            return await (await _moviesCollection.FindAsync(_ => true)).ToListAsync();
        }

        public async Task AddActorToMovie(string actorId, string movieId)
        {
            await _moviesCollection.UpdateOneAsync(movie => movie.Id == movieId,
                Builders<MovieDto>.Update.Push(movie => movie.StarringActors, new StarringActorDto(actorId)));
        }

        public MovieDto GetById(string id)
        {
            return _moviesCollection.FindSync(movie => movie.Id == id).FirstOrDefault();
        }

        public async Task Remove(string movieId)
        {
            await _moviesCollection.DeleteOneAsync(movie => movie.Id == movieId);
        }

        public async Task<IEnumerable<MovieDto>> GetByYear(int year)
        {
            return await (await _moviesCollection.FindAsync(movie => movie.Year == year)).ToListAsync();
        }

        public async Task<IEnumerable<MovieDto>> GetByActor(string actorId)
        {
            return await (await _moviesCollection.FindAsync(movie => movie.StarringActors
                    .Any(actor => actor.Id == actorId)))
                .ToListAsync();
        }

        public async Task<IEnumerable<string>> GetActorsStarringInMovie(string movieId)
        {
            return (await _moviesCollection.Find(movie => movie.Id == movieId)
                    .Project(movie => movie.StarringActors)
                    .SingleOrDefaultAsync())?
                .Select(starringActor => starringActor.Id)
                ?? new string[0];
        }

        public async Task RemoveActorFromStarringActors(string actorId)
        {
            await _moviesCollection.UpdateManyAsync(_ => true,
                Builders<MovieDto>.Update.Pull(movie => movie.StarringActors, new StarringActorDto(actorId)));
        }

        public async Task Update(MovieDto movieDto)
        {
            await _moviesCollection.ReplaceOneAsync(movie => movie.Id == movieDto.Id, movieDto);
        }

        private void SetupIndexes()
        {
            _moviesCollection.Indexes.CreateOne(
                new CreateIndexModel<MovieDto>(Builders<MovieDto>.IndexKeys.Ascending(f => f.Year)));
        }
    }
}