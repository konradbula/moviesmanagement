﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using Newtonsoft.Json;
using IActionResult = Microsoft.AspNetCore.Mvc.IActionResult;

namespace MoviesManagement.Inbound.Adapters
{
    public class RestOperationResult : IOperationResult
    {
        public IActionResult Response { get; private set; }

        public static RestOperationResult Create()
        {
            return new RestOperationResult();
        }

        public void NotifyAboutCreated()
        {
            Response = new StatusCodeResult(StatusCodes.Status201Created);
        }

        public void NotifyAboutCreated(MovieDto movieDto)
        {
            SetResponseFor(movieDto, StatusCodes.Status201Created);
        }

        public void NotifyAboutCreated(ActorDto actorDto)
        {
            SetResponseFor(actorDto, StatusCodes.Status201Created);
        }

        public void NotifyAboutNotUpdating()
        {
            Response = new StatusCodeResult(StatusCodes.Status304NotModified);
        }

        public void NotifyAboutValidationFailed()
        {
            Response = new StatusCodeResult(StatusCodes.Status400BadRequest);
        }

        public void NotifyAboutUpdate()
        {
            Response = new StatusCodeResult(StatusCodes.Status200OK);
        }
        
        public void NotifyAboutUpdate(ActorDto actorDto)
        {
            SetResponseFor(actorDto, StatusCodes.Status200OK);
        }
        
        public void NotifyAboutUpdate(MovieDto movieDto)
        {
            SetResponseFor(movieDto, StatusCodes.Status200OK);
        }

        public void NotifyAboutQuery(IEnumerable<ActorDto> actorDtos)
        {
            SetResponseFor(actorDtos);
        }
        
        public void NotifyAboutQuery(IEnumerable<MovieDto> movieDtos)
        {
            SetResponseFor(movieDtos);
        }
        
        private void SetResponseFor(object responseDto)
        {
            SetResponseFor(ResultFrom(responseDto), StatusCodes.Status200OK);
        }

        private void SetResponseFor(object dto, int statusCode)
        {
            Response = new JsonResult(dto,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                })
            {
                StatusCode = statusCode
            };
        }

        private static object ResultFrom(object responseDto)
        {
            return responseDto ?? new {wrapped = responseDto};
        }
    }
}