﻿using System.Threading.Tasks;

namespace MoviesManagement.Inbound.Ports.Requests
{
    public interface IInboundRequest
    {
        Task Handle();
    }
}