﻿namespace MoviesManagement.Inbound.Ports.Requests
{
    public interface IQueryRequestFactory
    {
        IInboundRequest CreateAllActors(IOperationResult result);
        IInboundRequest CreateAllMovies(IOperationResult result);
        IInboundRequest CreateMoviesByActor(string actorId, IOperationResult result);
        IInboundRequest CreateMoviesByYear(int year, IOperationResult result);
        IInboundRequest CreateActorsStarringInMovie(string movieId, IOperationResult result);
    }
}