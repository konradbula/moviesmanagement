﻿using MoviesManagement.Inbound.Ports.Dto;

namespace MoviesManagement.Inbound.Ports.Requests
{
    public interface IUpdateRequestFactory
    {
        IInboundRequest CreateAddActor(ActorDto actorDto, IOperationResult result);
        IInboundRequest CreateLinkExistingActorToExistingMovie(string actorId, string movieId, IOperationResult result);
        IInboundRequest CreateLinkExistingMovieToExistingActor(string movieId, string actorId, IOperationResult result);
        IInboundRequest CreateAddMovie(MovieDto movieDto, IOperationResult result);
        IInboundRequest CreateRemoveMovie(string movieId, IOperationResult result);
        IInboundRequest CreateRemoveActor(string actorId, IOperationResult result);
        IInboundRequest CreateUpdateMovie(MovieDto movieDto, IOperationResult result);
        IInboundRequest CreateUpdateActor(ActorDto actorDto, IOperationResult result);
    }
}