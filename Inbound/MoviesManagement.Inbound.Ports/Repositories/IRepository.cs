using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesManagement.Inbound.Ports.Repositories
{
    public interface IRepository<T>
    {
        Task Insert(T entity);
        Task<IEnumerable<T>> GetAll();
    }
}