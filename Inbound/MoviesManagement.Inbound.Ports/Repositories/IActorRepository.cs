using System.Collections.Generic;
using System.Threading.Tasks;
using MoviesManagement.Inbound.Ports.Dto;

namespace MoviesManagement.Inbound.Ports.Repositories
{
    public interface IActorRepository : IRepository<ActorDto>
    {
        ActorDto GetById(string id);
        Task Remove(string actorId);
        Task<IEnumerable<ActorDto>> GetByIds(IEnumerable<string> actorIds);
        Task RemoveMovieFromFilmography(string movieId);
        Task Update(ActorDto actorDto);
        Task AddMovieToActor(string movieId, string actorId);
    }
}