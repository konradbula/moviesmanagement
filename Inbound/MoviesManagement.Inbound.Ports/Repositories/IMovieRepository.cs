using System.Collections.Generic;
using System.Threading.Tasks;
using MoviesManagement.Inbound.Ports.Dto;

namespace MoviesManagement.Inbound.Ports.Repositories
{
    public interface IMovieRepository : IRepository<MovieDto>
    {
        Task AddActorToMovie(string actorId, string movieId);
        MovieDto GetById(string id);
        Task Remove(string movieId);
        Task<IEnumerable<MovieDto>> GetByYear(int year);
        Task<IEnumerable<MovieDto>> GetByActor(string actorId);
        Task<IEnumerable<string>> GetActorsStarringInMovie(string movieId);
        Task RemoveActorFromStarringActors(string actorId);
        Task Update(MovieDto movieDto);
    }
}