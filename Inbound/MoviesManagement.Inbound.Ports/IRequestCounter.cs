﻿namespace MoviesManagement.Inbound.Ports
{
    public interface IRequestCounter
    {
        void Increment(string requestName);
    }
}