﻿using System.Collections.Generic;
using MoviesManagement.Inbound.Ports.Dto;

namespace MoviesManagement.Inbound.Ports
{
    public interface IOperationResult
    {
        void NotifyAboutCreated();
        void NotifyAboutCreated(MovieDto movieDto);
        void NotifyAboutCreated(ActorDto actorDto);
        void NotifyAboutQuery(IEnumerable<ActorDto> actorDtos);
        void NotifyAboutQuery(IEnumerable<MovieDto> movieDtos);
        void NotifyAboutNotUpdating();
        void NotifyAboutValidationFailed();
        void NotifyAboutUpdate();
        void NotifyAboutUpdate(ActorDto actorDto);
        void NotifyAboutUpdate(MovieDto movieDto);
    }
}