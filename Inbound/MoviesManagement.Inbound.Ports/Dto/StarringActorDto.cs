using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace MoviesManagement.Inbound.Ports.Dto
{
    public class StarringActorDto
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; private set; }

        public StarringActorDto(string id)
        {
            Id = id;
        }

        protected bool Equals(StarringActorDto other)
        {
            return string.Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((StarringActorDto) obj);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }
    }
}