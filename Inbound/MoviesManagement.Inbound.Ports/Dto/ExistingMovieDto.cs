namespace MoviesManagement.Inbound.Ports.Dto
{
    public class ExistingMovieDto
    {
        public string Id { get; }

        public ExistingMovieDto(string id)
        {
            Id = id;
        }

        protected bool Equals(ExistingActorDto other)
        {
            return string.Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ExistingActorDto) obj);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }
    }
}