using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace MoviesManagement.Inbound.Ports.Dto
{
    public class ActorDto
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; private set; }

        [BsonElement]
        [BsonRequired]
        public string FirstName { get; }

        [BsonElement]
        [BsonRequired]
        public string LastName { get; }

        [BsonElement]
        [BsonDateTimeOptions(Kind = DateTimeKind.Unspecified)]
        public DateTime Birthday { get; }

        [BsonElement]
        public IEnumerable<FilmographyDto> Filmography { get; }

        [BsonConstructor]
        public ActorDto(string id,
            string firstName,
            string lastName,
            DateTime birthday,
            IEnumerable<FilmographyDto> filmography)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Filmography = filmography ?? new FilmographyDto[0];
        }

        public override string ToString() => $"[id: {Id}, FirstName: {FirstName}, LastName: {LastName}, Birthday: {Birthday}]";

        protected bool Equals(ActorDto other)
        {
            return string.Equals(Id, other.Id) && string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName) && Birthday.Equals(other.Birthday) && Equals(Filmography, other.Filmography);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ActorDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Birthday.GetHashCode();
                hashCode = (hashCode * 397) ^ (Filmography != null ? Filmography.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}