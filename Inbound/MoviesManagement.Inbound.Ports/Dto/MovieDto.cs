using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace MoviesManagement.Inbound.Ports.Dto
{
    public class MovieDto
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; private set; }

        [BsonElement]
        public string Title { get; }

        [BsonElement]
        public int Year { get; }

        [BsonElement]
        public string Genre { get; }

        [BsonElement]
        public IEnumerable<StarringActorDto> StarringActors { get; }

        [BsonConstructor]
        public MovieDto(string id, 
            string title,
            int year,
            string genre,
            IEnumerable<StarringActorDto> starringActors)
        {
            Title = title;
            Year = year;
            Genre = genre;
            StarringActors = starringActors ?? new StarringActorDto[0];
            Id = id;
        }

        public override string ToString() => $"[id: {Id}, Title: {Title}, Year: {Year}, Genre: {Genre}]";

        protected bool Equals(MovieDto other)
        {
            return string.Equals(Id, other.Id) && string.Equals(Title, other.Title) && Year == other.Year && string.Equals(Genre, other.Genre) && Equals(StarringActors, other.StarringActors);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MovieDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Title != null ? Title.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Year;
                hashCode = (hashCode * 397) ^ (Genre != null ? Genre.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (StarringActors != null ? StarringActors.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}