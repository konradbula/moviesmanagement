using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoviesManagement.Inbound.Ports.Dto;
using TddXt.AnyRoot.Collections;
using Xunit;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.Inbound.Adapters.UnitTests
{
    [Collection("MoviesManagement.Inbound.Adapters.UnitTests")]
    public class RestOperationResultUnitTests
    {
        [Fact]
        public void ShouldNotifyAboutCreated()
        {
            //GIVEN
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutCreated();

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<StatusCodeResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status201Created);
        }
        
        [Fact]
        public void ShouldNotifyAboutCreatedActor()
        {
            //GIVEN
            var actorDto = Any.Instance<ActorDto>();
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutCreated(actorDto);

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status201Created);
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.Value.Should().Be(actorDto);
        }
        
        [Fact]
        public void ShouldNotifyAboutCreatedMovie()
        {
            //GIVEN
            var movieDto = Any.Instance<MovieDto>();
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutCreated(movieDto);

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status201Created);
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.Value.Should().Be(movieDto);
        }
        
        [Fact]
        public void ShouldNotifyAboutQueryActorsDtos()
        {
            //GIVEN
            var actorDtos = Any.Enumerable<ActorDto>();
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutQuery(actorDtos);

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status200OK);
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.Value.Should().Be(actorDtos);
        }
        
        [Fact]
        public void ShouldNotifyAboutQueryMoviesDtos()
        {
            //GIVEN
            var movieDtos = Any.Enumerable<MovieDto>();
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutQuery(movieDtos);

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status200OK);
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.Value.Should().Be(movieDtos);
        }

        [Fact]
        public void ShouldNotifyAboutUpdate()
        {
            //GIVEN
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutUpdate();

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<StatusCodeResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status200OK);
        }
        
        [Fact]
        public void ShouldNotifyAboutUpdateActor()
        {
            //GIVEN
            var actorDto = Any.Instance<ActorDto>();
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutUpdate(actorDto);

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status200OK);
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.Value.Should().Be(actorDto);
        }
        
        [Fact]
        public void ShouldNotifyAboutUpdateMovie()
        {
            //GIVEN
            var movieDto = Any.Instance<MovieDto>();
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutUpdate(movieDto);

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status200OK);
            restOperationResult.Response.Should().BeOfType<JsonResult>()
                .Subject.Value.Should().Be(movieDto);
        }
        
        [Fact]
        public void ShouldNotifyAboutNotUpdating()
        {
            //GIVEN
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutNotUpdating();

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<StatusCodeResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status304NotModified);
        }
        
        [Fact]
        public void ShouldNotifyAboutValidationFailed()
        {
            //GIVEN
            var restOperationResult = new RestOperationResult();

            //WHEN
            restOperationResult.NotifyAboutValidationFailed();

            //THEN
            restOperationResult.Response.Should().NotBeNull();
            restOperationResult.Response.Should().BeOfType<StatusCodeResult>()
                .Subject.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }
    }
}