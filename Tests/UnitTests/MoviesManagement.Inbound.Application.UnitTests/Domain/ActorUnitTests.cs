using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot.Collections;
using TddXt.AnyRoot.Strings;
using TddXt.AnyRoot.Time;
using Xunit;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.Inbound.Application.UnitTests.Domain
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class ActorUnitTests
    {
        [Fact]
        public void ShouldSayThatActorExists()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            var result = actor.Exists();

            //THEN
            result.Should().BeTrue();
        }

        [Fact]
        public void ShouldSayThatActorNotExists()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var actorDto = NotExistActorDto();
            var movies = Any.Enumerable<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            var result = actor.Exists();

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public async void ShouldCorrectlyInsert()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Substitute.For<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            await actor.Insert();

            //THEN
            await actorRepository.Received().Insert(actorDto);
            operationResult.Received().NotifyAboutCreated(actorDto);
        }

        [Fact]
        public async void ShouldCorrectlyUpdate()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Substitute.For<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            await actor.Update();

            //THEN
            await actorRepository.Received().Update(actorDto);
            operationResult.Received().NotifyAboutUpdate(actorDto);
        }
        
        [Fact]
        public void ShouldSayThatHasAlreadyMovie()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Substitute.For<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var movie = Substitute.For<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            movie.HasAlready(Arg.Is<IEnumerable<string>>(filmography =>
                    filmography.SequenceEqual(FilmographyIdsFrom(actorDto))))
                .Returns(true);

            //WHEN
            var result = actor.HasAlready(movie);

            //THEN
            result.Should().BeTrue();
        }
        
        [Fact]
        public void ShouldSayThatHasNotAlreadyMovie()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Substitute.For<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var movie = Substitute.For<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            movie.HasAlready(Arg.Is<IEnumerable<string>>(filmography =>
                    filmography.SequenceEqual(FilmographyIdsFrom(actorDto))))
                .Returns(false);

            //WHEN
            var result = actor.HasAlready(movie);

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public async void ShouldCorrectlyAppendToMovie()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var movie = Substitute.For<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            await actor.AppendTo(movie);

            //THEN
            await movie.Received().Append(actorDto.Id);
            operationResult.Received().NotifyAboutUpdate();
        }
        
        [Fact]
        public async void ShouldCorrectlyAppendMovieId()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Substitute.For<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var movieId = Any.String();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            await actor.Append(movieId);

            //THEN
            await actorRepository.Received().AddMovieToActor(movieId, actorDto.Id);
        }

        [Fact]
        public void ShouldSayThatHasAlreadyOneOfThemActors()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var actorIds = new[] { actorDto.Id };
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            var result = actor.HasAlready(actorIds);

            //THEN
            result.Should().BeTrue();
        }

        [Fact]
        public void ShouldSayThatNotHasOfThemActors()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movies = Any.Enumerable<IMovie>();
            var actorIds = new[] { Any.String() };
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            var result = actor.HasAlready(actorIds);

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public void ShouldSayThatIsValid()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var actorDto = Any.Instance<ActorDto>();
            var movie = Substitute.For<IMovie>();
            var movies = new[] {movie};
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            movie.Exists().Returns(true);

            //WHEN
            var result = actor.IsValid();

            //THEN
            result.Should().BeTrue();
        }

        [Fact]
        public void ShouldSayThatIsValidWhenHasNotAnyExistingMovies()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var actorDto = NewActorDto(Any.String(), Any.String());
            var movies = new IMovie [0];
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            var result = actor.IsValid();

            //THEN
            result.Should().BeTrue();
        }

        [Fact]
        public void ShouldSayThatIsInvalidDueToFirstNameIsEmpty()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var firstName = string.Empty;
            var actorDto = NewActorDto(firstName, Any.String());
            var movies = Any.Enumerable<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            var result = actor.IsValid();

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public void ShouldSayThatIsInvalidDueToLastNameIsEmpty()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var lastName = Any.String();
            var actorDto = NewActorDto(Any.String(), lastName);
            var movies = Any.Enumerable<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            var result = actor.IsValid();

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public void ShouldSayThatIsInvalidDueToNotHasAnyExistingMovies()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var actorDto = NewActorDto(Any.String(), Any.String());
            var movie = Substitute.For<IMovie>();
            var movies = new[] {movie};
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            movie.Exists().Returns(false);

            //WHEN
            var result = actor.IsValid();

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public void ShouldNotifyAboutValidationFailed()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = NewActorDto(Any.String(), Any.String());
            var movies = Any.Enumerable<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            actor.NotifyAboutValidationFailed();

            //THEN
            operationResult.Received().NotifyAboutValidationFailed();
        }

        [Fact]
        public void NotifyThatAlreadyExistsMovie()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var actorDto = NewActorDto(Any.String(), Any.String());
            var movies = Any.Enumerable<IMovie>();
            var movie = Substitute.For<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            actor.NotifyThatAlreadyExists(movie);

            //THEN
            movie.Received().NotifyThatAlreadyExists(actorDto.ToString());
        }

        [Fact]
        public void NotifyThatAlreadyExistsMovieAsText()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = NewActorDto(Any.String(), Any.String());
            var movies = Any.Enumerable<IMovie>();
            var movie = Any.String();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            actor.NotifyThatAlreadyExists(movie);

            //THEN
            operationResult.Received().NotifyAboutNotUpdating();
        }

        [Fact]
        public void NotifyThatNotExists()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Actor>>();
            var actorRepository = Any.Instance<IActorRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var actorDto = NewActorDto(Any.String(), Any.String());
            var movies = Any.Enumerable<IMovie>();
            var actor = new Actor(logger, operationResult, actorDto, movies, actorRepository);

            //WHEN
            actor.NotifyThatNotExists();

            //THEN
            operationResult.Received().NotifyAboutNotUpdating();
        }

        private static ActorDto NewActorDto(string firstName, string lastName)
        {
            return new ActorDto(Any.String(),
                firstName,
                lastName,
                Any.DateTime(),
                Any.Enumerable<FilmographyDto>());
        }
        
        private static IEnumerable<string> FilmographyIdsFrom(ActorDto actorDto)
        {
            return actorDto.Filmography
                .Select(filmography =>
                    filmography.Id);
        }

        private static ActorDto NotExistActorDto()
        {
            return null;
        }
    }
}