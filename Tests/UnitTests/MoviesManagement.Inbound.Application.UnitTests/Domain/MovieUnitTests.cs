using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot.Collections;
using TddXt.AnyRoot.Numbers;
using TddXt.AnyRoot.Strings;
using Xunit;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.Inbound.Application.UnitTests.Domain
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class MovieUnitTests
    {
        [Fact]
        public void ShouldSayThatActorExists()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            var result = movie.Exists();

            //THEN
            result.Should().BeTrue();
        }

        [Fact]
        public async void ShouldCorrectlyInsert()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Substitute.For<IMovieRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            await movie.Insert();

            //THEN
            await movieRepository.Received().Insert(movieDto);
            operationResult.Received().NotifyAboutCreated(movieDto);
        }

        [Fact]
        public async void ShouldCorrectlyUpdate()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Substitute.For<IMovieRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            await movie.Update();

            //THEN
            await movieRepository.Received().Update(movieDto);
            operationResult.Received().NotifyAboutUpdate(movieDto);
        }

        [Fact]
        public void ShouldSayThatHasAlreadyActor()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var actor = Substitute.For<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            actor.HasAlready(Arg.Is<IEnumerable<string>>(starringActors =>
                    starringActors.SequenceEqual(StarringActorIdsFrom(movieDto))))
                .Returns(true);

            //WHEN
            var result = movie.HasAlready(actor);

            //THEN
            result.Should().BeTrue();
        }

        [Fact]
        public void ShouldSayThatHasNotAlreadyActor()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var actor = Substitute.For<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            actor.HasAlready(Arg.Is<IEnumerable<string>>(starringActors =>
                    starringActors.SequenceEqual(StarringActorIdsFrom(movieDto))))
                .Returns(false);

            //WHEN
            var result = movie.HasAlready(actor);

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public void ShouldSayThatIsValid()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var movieDto = NewMovieDto(HasYearLessThanCurrent());
            var actor = Substitute.For<IActor>();
            var actors = new[] { actor };
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            actor.Exists().Returns(true);

            //WHEN
            var result = movie.IsValid();

            //THEN
            result.Should().BeTrue();
        }

        [Fact]
        public void ShouldSayThatIsInvalidDueToYearIsGreaterThanCurrent()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var movieDto = NewMovieDto(HasYearGreaterThanCurrent());
            var actor = Substitute.For<IActor>();
            var actors = new[] { actor };
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            actor.Exists().Returns(true);

            //WHEN
            var result = movie.IsValid();

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public void ShouldSayThatIsInvalidDueToNotHasAnyActor()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var movieDto = NewMovieDto(HasYearGreaterThanCurrent());
            var actors = new IActor[0];
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            var result = movie.IsValid();

            //THEN
            result.Should().BeFalse();
        }
        
        [Fact]
        public async void ShouldCorrectlyAppendActorId()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Substitute.For<IMovieRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var actorId = Any.String();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            await movie.Append(actorId);

            //THEN
            await movieRepository.Received().AddActorToMovie(actorId, movieDto.Id);
        }
        
        [Fact]
        public async void ShouldCorrectlyAppendToActor()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var actor = Substitute.For<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            await movie.AppendTo(actor);

            //THEN
            await actor.Received().Append(movieDto.Id);
            operationResult.Received().NotifyAboutUpdate();
        }

        [Fact]
        public void ShouldSayThatIsInvalidDueToNotHasAnyExistingActor()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Any.Instance<IOperationResult>();
            var movieDto = NewMovieDto(HasYearGreaterThanCurrent());
            var actor = Substitute.For<IActor>();
            var actors = new[] { actor };
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            actor.Exists().Returns(false);

            //WHEN
            var result = movie.IsValid();

            //THEN
            result.Should().BeFalse();
        }

        [Fact]
        public void ShouldNotifyAboutValidationFailed()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            movie.NotifyAboutValidationFailed();

            //THEN
            operationResult.Received().NotifyAboutValidationFailed();
        }

        [Fact]
        public void ShouldNotifyThatAlreadyExistsActor()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actor = Substitute.For<IActor>();
            var actors = Any.Enumerable<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            movie.NotifyThatAlreadyExists(actor);

            //THEN
            actor.Received().NotifyThatAlreadyExists(movieDto.ToString());
        }

        [Fact]
        public void ShouldNotifyThatAlreadyExistsActorAsText()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actor = Any.String();
            var actors = Any.Enumerable<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            movie.NotifyThatAlreadyExists(actor);

            //THEN
            operationResult.Received().NotifyAboutNotUpdating();
        }

        [Fact]
        public void ShouldNotifyAboutNotUpdating()
        {
            //GIVEN
            var logger = Any.Instance<ILogger<Movie>>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var operationResult = Substitute.For<IOperationResult>();
            var movieDto = Any.Instance<MovieDto>();
            var actors = Any.Enumerable<IActor>();
            var movie = new Movie(logger, operationResult, movieDto, actors, movieRepository);

            //WHEN
            movie.NotifyThatNotExists();

            //THEN
            operationResult.Received().NotifyAboutNotUpdating();
        }

        private static int HasYearGreaterThanCurrent()
        {
            return DateTime.MaxValue.Year;
        }

        private static int HasYearLessThanCurrent()
        {
            return DateTime.MinValue.Year;
        }

        private static MovieDto NewMovieDto(int year)
        {
            return new MovieDto(Any.String(),
                Any.String(),
                year,
                Any.String(),
                Any.Enumerable<StarringActorDto>());
        }

        private static IEnumerable<string> StarringActorIdsFrom(MovieDto movieDto)
        {
            return movieDto.StarringActors
                .Select(starringActor =>
                    starringActor.Id);
        }
    }
}