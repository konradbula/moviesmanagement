using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Application.Requests.Creates;
using NSubstitute;
using TddXt.AnyRoot;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Creates
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class AddMovieRequestUnitTests
    {
        [Fact]
        public async void ShouldSayThatValidationFailed()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<AddMovieRequest>>();
            var movie = Substitute.For<IMovie>();
            var addMovieRequest = new AddMovieRequest(logger,
                movie);

            movie.IsValid().Returns(false);
            
            //WHEN
            await addMovieRequest.Handle();

            //THEN
            movie.Received().NotifyAboutValidationFailed();
            await movie.DidNotReceive().Insert();
        }
        
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<AddMovieRequest>>();
            var movie = Substitute.For<IMovie>();
            var addMovieRequest = new AddMovieRequest(logger,
                movie);

            movie.IsValid().Returns(true);
            
            //WHEN
            await addMovieRequest.Handle();

            //THEN
            movie.DidNotReceive().NotifyAboutValidationFailed();
            await movie.Received().Insert();
        }
    }
}