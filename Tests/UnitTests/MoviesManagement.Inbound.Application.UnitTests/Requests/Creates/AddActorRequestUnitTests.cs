using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Application.Requests.Creates;
using NSubstitute;
using TddXt.AnyRoot;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Creates
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class AddActorRequestUnitTests
    {
        [Fact]
        public async void ShouldSayThatValidationFailed()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<AddActorRequest>>();
            var actor = Substitute.For<IActor>();
            var addActorRequest = new AddActorRequest(logger,
                actor);

            actor.IsValid().Returns(false);

            //WHEN
            await addActorRequest.Handle();

            //THEN
            actor.Received().NotifyAboutValidationFailed();
        }

        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<AddActorRequest>>();
            var actor = Substitute.For<IActor>();
            var addActorRequest = new AddActorRequest(logger,
                actor);

            actor.IsValid().Returns(true);
            
            //WHEN
            await addActorRequest.Handle();

            //THEN
            actor.DidNotReceive().NotifyAboutValidationFailed();
            await actor.Received().Insert();
        }
    }
}