using MoviesManagement.Inbound.Application.Requests;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Requests;
using NSubstitute;
using TddXt.AnyRoot.Strings;
using Xunit;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class InboundRequestCounterUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var inboundRequest = Substitute.For<IInboundRequest>();
            var requestCounter = Substitute.For<IRequestCounter>();
            var requestName = Any.String();
            var inboundRequestCounter = new InboundRequestCounter(inboundRequest,
                requestCounter,
                requestName);
            
            //WHEN
            await inboundRequestCounter.Handle();

            //THEN
            requestCounter.Received().Increment(requestName);
            await inboundRequest.Received().Handle();
        }
    }
}