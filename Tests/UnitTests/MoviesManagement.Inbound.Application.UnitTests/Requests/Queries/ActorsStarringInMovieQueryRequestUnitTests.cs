using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot;
using TddXt.AnyRoot.Collections;
using TddXt.AnyRoot.Strings;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Queries
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class ActorsStarringInMovieQueryRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<ActorsStarringInMovieQueryRequest>>();
            var movieId = Root.Any.String();
            var actorRepository = Substitute.For<IActorRepository>();
            var movieRepository = Substitute.For<IMovieRepository>();
            var result = Substitute.For<IOperationResult>();
            var actorIds = Root.Any.Enumerable<string>();
            var actorDtos = Root.Any.Enumerable<ActorDto>();
            var actorsStarringInMovieQueryRequest = new ActorsStarringInMovieQueryRequest(logger, 
                movieId, 
                actorRepository, 
                movieRepository,
                result);

            movieRepository.GetActorsStarringInMovie(movieId).Returns(Task.FromResult(actorIds));
            actorRepository.GetByIds(actorIds).Returns(Task.FromResult(actorDtos));

            //WHEN
            await actorsStarringInMovieQueryRequest.Handle();

            //THEN
            result.Received().NotifyAboutQuery(actorDtos);
        }
    }
}