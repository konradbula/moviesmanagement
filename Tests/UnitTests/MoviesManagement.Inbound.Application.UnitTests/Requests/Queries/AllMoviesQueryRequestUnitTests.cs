using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot;
using TddXt.AnyRoot.Collections;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Queries
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class AllMoviesQueryRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<AllMoviesQueryRequest>>();
            var moviesRepository = Substitute.For<IMovieRepository>();
            var result = Substitute.For<IOperationResult>();
            var movieDtos = Root.Any.Enumerable<MovieDto>();
            var allMoviesQueryRequest = new AllMoviesQueryRequest(logger,
                moviesRepository,
                result);

            moviesRepository.GetAll().Returns(Task.FromResult(movieDtos));
            
            //WHEN
            await allMoviesQueryRequest.Handle();

            //THEN
            result.Received().NotifyAboutQuery(movieDtos);
        }
    }
}