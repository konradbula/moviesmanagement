using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot;
using TddXt.AnyRoot.Collections;
using TddXt.AnyRoot.Strings;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Queries
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class MoviesByActorQueryRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<MoviesByActorQueryRequest>>();
            var moviesRepository = Substitute.For<IMovieRepository>();
            var result = Substitute.For<IOperationResult>();
            var movieDtos = Root.Any.Enumerable<MovieDto>();
            var actorId = Root.Any.String();
            var moviesByActorQueryRequest = new MoviesByActorQueryRequest(logger,
                actorId,
                moviesRepository,
                result);

            moviesRepository.GetByActor(actorId).Returns(Task.FromResult(movieDtos));
            
            //WHEN
            await moviesByActorQueryRequest.Handle();

            //THEN
            result.Received().NotifyAboutQuery(movieDtos);
        }
    }
}