using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot;
using TddXt.AnyRoot.Collections;
using TddXt.AnyRoot.Numbers;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Queries
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class MoviesByYearQueryRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<MoviesByYearQueryRequest>>();
            var moviesRepository = Substitute.For<IMovieRepository>();
            var result = Substitute.For<IOperationResult>();
            var movieDtos = Root.Any.Enumerable<MovieDto>();
            var year = Root.Any.Integer();
            var moviesByYearQueryRequest = new MoviesByYearQueryRequest(logger,
                year,
                moviesRepository,
                result);

            moviesRepository.GetByYear(year).Returns(Task.FromResult(movieDtos));
            
            //WHEN
            await moviesByYearQueryRequest.Handle();

            //THEN
            result.Received().NotifyAboutQuery(movieDtos);
        }
    }
}