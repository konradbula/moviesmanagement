using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot;
using TddXt.AnyRoot.Collections;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Queries
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class AllActorsQueryRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<AllActorsQueryRequest>>();
            var actorRepository = Substitute.For<IActorRepository>();
            var result = Substitute.For<IOperationResult>();
            var actorDtos = Root.Any.Enumerable<ActorDto>();
            var addMovieRequest = new AllActorsQueryRequest(logger,
                actorRepository,
                result);

            actorRepository.GetAll().Returns(Task.FromResult(actorDtos));
            
            //WHEN
            await addMovieRequest.Handle();

            //THEN
            result.Received().NotifyAboutQuery(actorDtos);
        }
    }
}