using FluentAssertions;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests;
using MoviesManagement.Inbound.Application.Requests.Creates;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Application.Requests.Removes;
using MoviesManagement.Inbound.Application.Requests.Updates;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using TddXt.AnyRoot.Numbers;
using TddXt.AnyRoot.Strings;
using TddXt.XFluentAssert.Root;
using Xunit;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class InboundUpdateRequestFactoryUnitTests
    {
        [Fact]
        public void ShouldCorrectlyCreateAddActor()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var actorDto = Any.Instance<ActorDto>();
            var result = Any.Instance<IOperationResult>();
            var inboundUpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundUpdateRequestFactory.CreateAddActor(actorDto, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            //TODO: this must be commented on until the library will be fix regard to find dependent objects in collection
            //inboundUpdateRequestFactory.Should().DependOn<AddActorRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateAddMovie()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var movieDto = Any.Instance<MovieDto>();
            var result = Any.Instance<IOperationResult>();
            var inboundUpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundUpdateRequestFactory.CreateAddMovie(movieDto, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            //TODO: this must be commented on until the library will be fix regard to find dependent objects in collection
            //inboundUpdateRequestFactory.Should().DependOn<UpdateActorRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateRemoveActor()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var actorId = Any.Instance<string>();
            var result = Any.Instance<IOperationResult>();
            var inboundUpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundUpdateRequestFactory.CreateRemoveActor(actorId, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            inboundRequest.Should().DependOn<RemoveActorRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateRemoveMovie()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var movieId = Any.Instance<string>();
            var result = Any.Instance<IOperationResult>();
            var inboundUpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundUpdateRequestFactory.CreateRemoveMovie(movieId, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            inboundRequest.Should().DependOn<RemoveMovieRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateUpdateActor()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var actorDto = Any.Instance<ActorDto>();
            var result = Any.Instance<IOperationResult>();
            var inboundUpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundUpdateRequestFactory.CreateUpdateActor(actorDto, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            //TODO: this must be commented on until the library will be fix regard to find dependent objects in collection
            //inboundUpdateRequestFactory.Should().DependOn<UpdateActorRequest>();
        }
        
        [Fact]
        public void ShouldCorrectlyCreateUpdateMovie()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var movieDto = Any.Instance<MovieDto>();
            var result = Any.Instance<IOperationResult>();
            var inboundUpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundUpdateRequestFactory.CreateUpdateMovie(movieDto, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            //TODO: this must be commented on until the library will be fix regard to find dependent objects in collection
            //inboundUpdateRequestFactory.Should().DependOn<UpdateMovieRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateLinkExistingActorToExistingMovie()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var actorId = Any.String();
            var movieId = Any.String();
            var result = Any.Instance<IOperationResult>();
            var inboundUpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundUpdateRequestFactory.CreateLinkExistingActorToExistingMovie(actorId, movieId, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            //TODO: this must be commented on until the library will be fix regard to find dependent objects in collection
            //inboundUpdateRequestFactory.Should().DependOn<LinkExistingActorToExistingMovieRequest>();
        }
        
        [Fact]
        public void ShouldCorrectlyCreateLinkExistingMovieToExistingActor()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var actorId = Any.String();
            var movieId = Any.String();
            var result = Any.Instance<IOperationResult>();
            var inboundUpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundUpdateRequestFactory.CreateLinkExistingMovieToExistingActor(movieId, actorId, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            //TODO: this must be commented on until the library will be fix regard to find dependent objects in collection
            //inboundUpdateRequestFactory.Should().DependOn<LinkExistingActorToExistingMovieRequest>();
        }
    }
}