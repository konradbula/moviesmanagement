using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Application.Requests.Updates;
using NSubstitute;
using TddXt.AnyRoot;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Updates
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class UpdateActorRequestUnitTests
    {
        [Fact]
        public async void ShouldSayThatValidationFailed()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<UpdateActorRequest>>();
            var actor = Substitute.For<IActor>();
            var updateActorRequest = new UpdateActorRequest(logger,
                actor);

            actor.IsValid().Returns(false);

            //WHEN
            await updateActorRequest.Handle();

            //THEN
            actor.Received().NotifyAboutValidationFailed();
        }

        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<UpdateActorRequest>>();
            var actor = Substitute.For<IActor>();
            var updateActorRequest = new UpdateActorRequest(logger,
                actor);

            actor.IsValid().Returns(true);
            
            //WHEN
            await updateActorRequest.Handle();

            //THEN
            actor.DidNotReceive().NotifyAboutValidationFailed();
            await actor.Received().Update();
        }
    }
}