using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Application.Requests.Updates;
using NSubstitute;
using TddXt.AnyRoot;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Updates
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class UpdateMovieRequestUnitTests
    {
        [Fact]
        public async void ShouldSayThatValidationFailed()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<UpdateMovieRequest>>();
            var movie = Substitute.For<IMovie>();
            var updateMovieRequest = new UpdateMovieRequest(logger,
                movie);

            movie.IsValid().Returns(false);
            
            //WHEN
            await updateMovieRequest.Handle();

            //THEN
            movie.Received().NotifyAboutValidationFailed();
        }
        
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<UpdateMovieRequest>>();
            var movie = Substitute.For<IMovie>();
            var updateMovieRequest = new UpdateMovieRequest(logger,
                movie);

            movie.IsValid().Returns(true);
            
            //WHEN
            await updateMovieRequest.Handle();

            //THEN
            movie.DidNotReceive().NotifyAboutValidationFailed();
            await movie.Received().Update();
        }
    }
}