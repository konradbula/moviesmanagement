using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Application.Requests.Updates;
using NSubstitute;
using TddXt.AnyRoot;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Updates
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class LinkExistingActorToExistingMovieRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<LinkExistingActorToExistingMovieRequest>>();
            var movie = Substitute.For<IMovie>();
            var actor = Substitute.For<IActor>();
            var linkExistingActorToExistingMovieRequest = new LinkExistingActorToExistingMovieRequest(
                logger,
                movie,
                actor);

            actor.Exists().Returns(true);
            movie.Exists().Returns(true);
            movie.HasAlready(actor).Returns(false);

            //WHEN
            await linkExistingActorToExistingMovieRequest.Handle();

            //THEN
            await actor.Received().AppendTo(movie);
        }

        [Fact]
        public async void ShouldSayThatActorNotExists()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<LinkExistingActorToExistingMovieRequest>>();
            var movie = Substitute.For<IMovie>();
            var actor = Substitute.For<IActor>();
            var linkExistingActorToExistingMovieRequest = new LinkExistingActorToExistingMovieRequest(
                logger,
                movie,
                actor);

            actor.Exists().Returns(false);

            //WHEN
            await linkExistingActorToExistingMovieRequest.Handle();

            //THEN
            actor.Received().NotifyThatNotExists();
            movie.DidNotReceive().NotifyThatNotExists();
            movie.DidNotReceive().NotifyThatAlreadyExists(Arg.Any<IActor>());
            await actor.DidNotReceive().AppendTo(movie);
        }

        [Fact]
        public async void ShouldSayThatMovieNotExists()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<LinkExistingActorToExistingMovieRequest>>();
            var movie = Substitute.For<IMovie>();
            var actor = Substitute.For<IActor>();
            var linkExistingActorToExistingMovieRequest = new LinkExistingActorToExistingMovieRequest(
                logger,
                movie,
                actor);

            actor.Exists().Returns(true);
            movie.Exists().Returns(false);

            //WHEN
            await linkExistingActorToExistingMovieRequest.Handle();

            //THEN
            actor.DidNotReceive().NotifyThatNotExists();
            movie.Received().NotifyThatNotExists();
            movie.DidNotReceive().NotifyThatAlreadyExists(Arg.Any<IActor>());
            await actor.DidNotReceive().AppendTo(movie);
        }

        [Fact]
        public async void ShouldSayThatActorAlreadyExistsInMovie()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<LinkExistingActorToExistingMovieRequest>>();
            var movie = Substitute.For<IMovie>();
            var actor = Substitute.For<IActor>();
            var linkExistingActorToExistingMovieRequest = new LinkExistingActorToExistingMovieRequest(
                logger,
                movie,
                actor);

            actor.Exists().Returns(true);
            movie.Exists().Returns(true);
            movie.HasAlready(actor).Returns(true);
            
            //WHEN
            await linkExistingActorToExistingMovieRequest.Handle();

            //THEN
            actor.DidNotReceive().NotifyThatNotExists();
            movie.DidNotReceive().NotifyThatNotExists();
            movie.Received().NotifyThatAlreadyExists(actor);
            await actor.DidNotReceive().AppendTo(movie);
        }
    }
}