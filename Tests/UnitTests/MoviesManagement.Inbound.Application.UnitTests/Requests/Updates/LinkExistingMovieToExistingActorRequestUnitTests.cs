using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Domain;
using MoviesManagement.Inbound.Application.Requests.Updates;
using NSubstitute;
using TddXt.AnyRoot;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Updates
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class LinkExistingMovieToExistingActorRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<LinkExistingMovieToExistingActorRequest>>();
            var movie = Substitute.For<IMovie>();
            var actor = Substitute.For<IActor>();
            var linkExistingMovieToExistingActorRequest = new LinkExistingMovieToExistingActorRequest(
                logger,
                movie,
                actor);

            actor.Exists().Returns(true);
            movie.Exists().Returns(true);
            movie.HasAlready(actor).Returns(false);

            //WHEN
            await linkExistingMovieToExistingActorRequest.Handle();

            //THEN
            await movie.Received().AppendTo(actor);
        }

        [Fact]
        public async void ShouldSayThatActorNotExists()
        {
            var logger = Root.Any.Instance<ILogger<LinkExistingMovieToExistingActorRequest>>();
            var movie = Substitute.For<IMovie>();
            var actor = Substitute.For<IActor>();
            var linkExistingMovieToExistingActorRequest = new LinkExistingMovieToExistingActorRequest(
                logger,
                movie,
                actor);

            actor.Exists().Returns(false);

            //WHEN
            await linkExistingMovieToExistingActorRequest.Handle();

            //THEN
            actor.Received().NotifyThatNotExists();
            movie.DidNotReceive().NotifyThatNotExists();
            movie.DidNotReceive().NotifyThatAlreadyExists(Arg.Any<IActor>());
            await movie.DidNotReceive().AppendTo(actor);
        }

        [Fact]
        public async void ShouldSayThatMovieNotExists()
        {
            var logger = Root.Any.Instance<ILogger<LinkExistingMovieToExistingActorRequest>>();
            var movie = Substitute.For<IMovie>();
            var actor = Substitute.For<IActor>();
            var linkExistingMovieToExistingActorRequest = new LinkExistingMovieToExistingActorRequest(
                logger,
                movie,
                actor);

            actor.Exists().Returns(true);
            movie.Exists().Returns(false);

            //WHEN
            await linkExistingMovieToExistingActorRequest.Handle();

            //THEN
            actor.DidNotReceive().NotifyThatNotExists();
            movie.Received().NotifyThatNotExists();
            movie.DidNotReceive().NotifyThatAlreadyExists(Arg.Any<IActor>());
            await movie.DidNotReceive().AppendTo(actor);
        }

        [Fact]
        public async void ShouldSayThatActorAlreadyExistsInMovie()
        {
            var logger = Root.Any.Instance<ILogger<LinkExistingMovieToExistingActorRequest>>();
            var movie = Substitute.For<IMovie>();
            var actor = Substitute.For<IActor>();
            var linkExistingMovieToExistingActorRequest = new LinkExistingMovieToExistingActorRequest(
                logger,
                movie,
                actor);

            actor.Exists().Returns(true);
            movie.Exists().Returns(true);
            actor.HasAlready(movie).Returns(true);

            //WHEN
            await linkExistingMovieToExistingActorRequest.Handle();

            //THEN
            actor.DidNotReceive().NotifyThatNotExists();
            movie.DidNotReceive().NotifyThatNotExists();
            actor.Received().NotifyThatAlreadyExists(movie);
            await movie.DidNotReceive().AppendTo(actor);
        }
    }
}