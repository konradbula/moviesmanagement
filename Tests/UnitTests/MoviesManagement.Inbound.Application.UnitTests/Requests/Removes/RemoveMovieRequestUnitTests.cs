using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests.Removes;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot;
using TddXt.AnyRoot.Strings;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Removes
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class RemoveMovieRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<RemoveMovieRequest>>();
            var actorRepository = Substitute.For<IActorRepository>();
            var movieRepository = Substitute.For<IMovieRepository>();
            var result = Root.Any.Instance<IOperationResult>();
            var movieId = Root.Any.String();
            var removeMovieRequest = new RemoveMovieRequest(logger,
                movieId,
                actorRepository,
                movieRepository,
                result);
            
            //WHEN
            await removeMovieRequest.Handle();

            //THEN
            await actorRepository.Received().RemoveMovieFromFilmography(movieId);
            await movieRepository.Received().Remove(movieId);
        }
    }
}