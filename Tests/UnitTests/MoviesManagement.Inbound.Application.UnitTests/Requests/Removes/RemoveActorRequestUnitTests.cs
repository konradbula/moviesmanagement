using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests.Removes;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot;
using TddXt.AnyRoot.Strings;
using Xunit;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests.Removes
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class RemoveActorRequestUnitTests
    {
        [Fact]
        public async void ShouldCorrectlyHandle()
        {
            //GIVEN
            var logger = Root.Any.Instance<ILogger<RemoveActorRequest>>();
            var actorRepository = Substitute.For<IActorRepository>();
            var movieRepository = Substitute.For<IMovieRepository>();
            var result = Root.Any.Instance<IOperationResult>();
            var actorId = Root.Any.String();
            var removeActorRequest = new RemoveActorRequest(logger,
                actorId,
                actorRepository,
                movieRepository,
                result);
            
            //WHEN
            await removeActorRequest.Handle();

            //THEN
            movieRepository.Received().RemoveActorFromStarringActors(actorId).Wait();
            actorRepository.Received().Remove(actorId).Wait();
        }
    }
}