using FluentAssertions;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests;
using MoviesManagement.Inbound.Application.Requests.Queries;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;
using TddXt.AnyRoot.Numbers;
using TddXt.AnyRoot.Strings;
using TddXt.XFluentAssert.Root;
using Xunit;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.Inbound.Application.UnitTests.Requests
{
    [Collection("MoviesManagement.Inbound.Application.UnitTests")]
    public class InboundQueryRequestFactoryUnitTests
    {
        [Fact]
        public void ShouldCorrectlyCreateAllActorsQuery()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var result = Any.Instance<IOperationResult>();
            var inboundQueryRequestFactory = new InboundQueryRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundQueryRequestFactory.CreateAllActors(result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            inboundRequest.Should().DependOn<AllActorsQueryRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateAllMoviesQuery()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var result = Any.Instance<IOperationResult>();
            var inboundQueryRequestFactory = new InboundQueryRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundQueryRequestFactory.CreateAllMovies(result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            inboundRequest.Should().DependOn<AllMoviesQueryRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateMoviesByActorQuery()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var actorId = Any.String();
            var result = Any.Instance<IOperationResult>();
            var inboundQueryRequestFactory = new InboundQueryRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundQueryRequestFactory.CreateMoviesByActor(actorId, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            inboundRequest.Should().DependOn<MoviesByActorQueryRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateMoviesByYearQuery()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var year = Any.Integer();
            var result = Any.Instance<IOperationResult>();
            var inboundQueryRequestFactory = new InboundQueryRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundQueryRequestFactory.CreateMoviesByYear(year, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            inboundRequest.Should().DependOn<MoviesByYearQueryRequest>();
        }

        [Fact]
        public void ShouldCorrectlyCreateActorsStarringInMovieQuery()
        {
            //GIVEN
            var requestCounter = Any.Instance<IRequestCounter>();
            var loggerFactory = Any.Instance<ILoggerFactory>();
            var actorsRepository = Any.Instance<IActorRepository>();
            var movieRepository = Any.Instance<IMovieRepository>();
            var movieId = Any.String();
            var result = Any.Instance<IOperationResult>();
            var inboundQueryRequestFactory = new InboundQueryRequestFactory(loggerFactory,
                requestCounter,
                actorsRepository,
                movieRepository);

            //WHEN
            var inboundRequest = inboundQueryRequestFactory.CreateActorsStarringInMovie(movieId, result);

            //THEN
            inboundRequest.Should().BeOfType<InboundRequestCounter>();
            inboundRequest.Should().DependOn<ActorsStarringInMovieQueryRequest>();
        }
    }
}