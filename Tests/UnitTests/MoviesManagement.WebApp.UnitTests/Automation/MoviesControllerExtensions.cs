using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using NSubstitute;
using TddXt.AnyRoot.Numbers;
using TddXt.AnyRoot.Strings;
using TddXt.AnyRoot.Time;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.WebApp.UnitTests.Automation
{
    public static class MoviesControllerExtensions
    {
        public static ActorDto NotExistingActor()
        {
            return null;
        }

        public static MovieDto NotExistingMovie()
        {
            return null;
        }

        public static MovieDto ExistingMovie(string movieId, string actorId)
        {
            return new MovieDto(movieId,
                Any.String(),
                Any.Integer(),
                Any.String(),
                new[] { new StarringActorDto(actorId) });
        }

        public static ActorDto ExistingActor(string actorId, string movieId)
        {
            return new ActorDto(actorId,
                Any.String(),
                Any.String(),
                Any.DateTime(),
                new[] { new FilmographyDto(movieId) });
        }

        public static ActorDto NewActorWithoutFilmography()
        {
            return new ActorDto(Any.String(),
                Any.String(),
                Any.String(),
                Any.DateTime(),
                null);
        }

        public static InboundUpdateRequestFactory NewInboundUpdateRequestFactory()
        {
            return NewInboundUpdateRequestFactory(Substitute.For<IActorRepository>(), Substitute.For<IMovieRepository>());
        }

        public static InboundUpdateRequestFactory NewInboundUpdateRequestFactory(IActorRepository actorRepository)
        {
            return NewInboundUpdateRequestFactory(actorRepository, Substitute.For<IMovieRepository>());
        }

        public static InboundUpdateRequestFactory NewInboundUpdateRequestFactory(IMovieRepository movieRepository)
        {
            return NewInboundUpdateRequestFactory(Substitute.For<IActorRepository>(), movieRepository);
        }

        public static InboundUpdateRequestFactory NewInboundUpdateRequestFactory(IActorRepository actorRepository,
            IMovieRepository movieRepository)
        {
            return new InboundUpdateRequestFactory(Any.Instance<ILoggerFactory>(), Any.Instance<IRequestCounter>(), actorRepository, movieRepository);
        }

        public static InboundQueryRequestFactory NewInboundQueryRequestFactory()
        {
            return NewInboundQueryRequestFactory(Substitute.For<IActorRepository>(), Substitute.For<IMovieRepository>());
        }

        public static InboundQueryRequestFactory NewInboundQueryRequestFactory(IActorRepository actorRepository)
        {
            return NewInboundQueryRequestFactory(actorRepository, Substitute.For<IMovieRepository>());
        }

        public static InboundQueryRequestFactory NewInboundQueryRequestFactory(IMovieRepository movieRepository)
        {
            return NewInboundQueryRequestFactory(Substitute.For<IActorRepository>(), movieRepository);
        }

        public static InboundQueryRequestFactory NewInboundQueryRequestFactory(IActorRepository actorRepository,
            IMovieRepository movieRepository)
        {
            return new InboundQueryRequestFactory(Any.Instance<ILoggerFactory>(), Any.Instance<IRequestCounter>(), actorRepository, movieRepository);
        }

        public static JsonResult JsonResultFrom(IActionResult actionResult)
        {
            return actionResult.Should().BeOfType<JsonResult>().Subject;
        }

        public static StatusCodeResult StatusCodeResultFrom(IActionResult actionResult)
        {
            return actionResult.Should().BeOfType<StatusCodeResult>().Subject;
        }

        public static int StatusCodeFrom(StatusCodeResult result)
        {
            return result.Should().BeOfType<StatusCodeResult>().Subject.StatusCode;
        }

        public static int? StatusCodeFrom(JsonResult result)
        {
            return result.Should().BeOfType<JsonResult>().Subject.StatusCode;
        }

        public static T ModelFrom<T>(IActionResult result)
        {
            return result.Should().BeOfType<JsonResult>().Subject.Value.Should().BeOfType<T>().Subject;
        }
    }
}