using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.WebApp.Controllers;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using TddXt.AnyRoot;
using TddXt.AnyRoot.Collections;
using TddXt.AnyRoot.Numbers;
using TddXt.AnyRoot.Strings;
using Xunit;
using static MoviesManagement.WebApp.UnitTests.Automation.MoviesControllerExtensions;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.WebApp.UnitTests.Controllers
{
    [Collection("MoviesManagement.WebApp.UnitTests")]
    public class MoviesQueryControllerUnitTests
    {
        [Fact]
        public async void ShouldReceivedResponseContainsAllActors()
        {
            //GIVEN
            var actorRepository = Substitute.For<IActorRepository>();
            var requestFactory = NewInboundQueryRequestFactory(actorRepository);
            var actorDtos = Any.Enumerable<ActorDto>();
            var moviesQueryController = new MoviesQueryController(requestFactory);

            actorRepository.GetAll().Returns(Task.FromResult(actorDtos));

            //WHEN
            var response = await moviesQueryController.GetAllActors();

            //THEN
            StatusCodeFrom(JsonResultFrom(response)).Should().Be(StatusCodes.Status200OK);
            ModelFrom<List<ActorDto>>(response).Should().BeEquivalentTo(actorDtos);
        }

        [Fact]
        public async void ShouldReceivedResponseContainsAllMovies()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var requestFactory = NewInboundQueryRequestFactory(movieRepository);
            var movieDtos = Any.Enumerable<MovieDto>();
            var moviesQueryController = new MoviesQueryController(requestFactory);

            movieRepository.GetAll().Returns(Task.FromResult(movieDtos));

            //WHEN
            var response = await moviesQueryController.GetAllMovies();

            //THEN
            StatusCodeFrom(JsonResultFrom(response)).Should().Be(StatusCodes.Status200OK);
            ModelFrom<List<MovieDto>>(response).Should().BeEquivalentTo(movieDtos);
        }

        [Fact]
        public async void ShouldReceivedResponseContainsMoviesBySpecifiedActor()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var requestFactory = NewInboundQueryRequestFactory(movieRepository);
            var movieDtos = Any.Enumerable<MovieDto>();
            var moviesQueryController = new MoviesQueryController(requestFactory);
            var actorId = Any.String();

            movieRepository.GetByActor(actorId).Returns(Task.FromResult(movieDtos));

            //WHEN
            var response = await moviesQueryController.GetMoviesByActor(actorId);;

            //THEN
            StatusCodeFrom(JsonResultFrom(response)).Should().Be(StatusCodes.Status200OK);
            ModelFrom<List<MovieDto>>(response).Should().BeEquivalentTo(movieDtos);
        }

        [Fact]
        public async void ShouldReceivedResponseContainsMoviesBySpecifiedYear()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var requestFactory = NewInboundQueryRequestFactory(movieRepository);
            var movieDtos = Any.Enumerable<MovieDto>();
            var year = Any.Integer();
            var moviesQueryController = new MoviesQueryController(requestFactory);

            movieRepository.GetByYear(year).Returns(Task.FromResult(movieDtos));

            //WHEN
            var response = await moviesQueryController.GetMoviesByYear(year);

            //THEN
            StatusCodeFrom(JsonResultFrom(response)).Should().Be(StatusCodes.Status200OK);
            ModelFrom<List<MovieDto>>(response).Should().BeEquivalentTo(movieDtos);
        }

        [Fact]
        public async void ShouldReceivedResponseContainsActorsStarringInSpecifiedMovie()
        {
            //GIVEN
            var actorRepository = Substitute.For<IActorRepository>();
            var movieRepository = Substitute.For<IMovieRepository>();
            var requestFactory = NewInboundQueryRequestFactory(actorRepository, movieRepository);
            var actorDtos = Any.Enumerable<ActorDto>();
            var MoviesQueryController = new MoviesQueryController(requestFactory);
            var movieId = Any.String();
            var actorsIds = Any.Enumerable<string>();

            movieRepository.GetActorsStarringInMovie(movieId).Returns(actorsIds);
            actorRepository.GetByIds(actorsIds).Returns(Task.FromResult(actorDtos));

            //WHEN
            var response = await MoviesQueryController.GetActorsStarringInMovie(movieId);

            //THEN
            StatusCodeFrom(JsonResultFrom(response)).Should().Be(StatusCodes.Status200OK);
            ModelFrom<List<ActorDto>>(response).Should().BeEquivalentTo(actorDtos);
        }

        [Fact]
        public async void ShouldNotCatchUnhandledException()
        {
            //GIVEN
            var actorRepository = Substitute.For<IActorRepository>();
            var requestFactory = NewInboundQueryRequestFactory(actorRepository);
            var moviesQueryController = new MoviesQueryController(requestFactory);
            var movieId = Any.String();
            var exception = Any.Exception();
            var actorsIds = Any.Enumerable<string>();

            actorRepository.GetByIds(actorsIds).Throws(exception);

            //WHEN - THEN
            (await moviesQueryController.GetActorsStarringInMovie(movieId))
                .Should().Throws(exception);
        }
    }
}