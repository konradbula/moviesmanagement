using FluentAssertions;
using Microsoft.AspNetCore.Http;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.WebApp.Controllers;
using MoviesManagement.WebApp.UnitTests.Automation;
using NSubstitute;
using TddXt.AnyRoot.Strings;
using Xunit;
using static MoviesManagement.WebApp.UnitTests.Automation.MoviesControllerExtensions;
using static TddXt.AnyRoot.Root;

namespace MoviesManagement.WebApp.UnitTests.Controllers
{
    [Collection("MoviesManagement.WebApp.UnitTests")]
    public class MoviesUpdateControllerUnitTests
    {
        [Fact]
        public async void ShouldReceivedResponseAboutAddedActor()
        {
            //GIVEN
            var actorDto = NewActorWithoutFilmography();
            var requestFactory = NewInboundUpdateRequestFactory();
            var moviesController = new MoviesUpdateController(requestFactory);

            //WHEN
            var response = await moviesController.AddActor(actorDto);

            //THEN
            StatusCodeFrom(JsonResultFrom(response)).Should().Be(StatusCodes.Status201Created);
            ModelFrom<ActorDto>(response).Should().Be(actorDto);
        }

        [Fact]
        public async void ShouldReceivedBadRequestWhenActorCouldNotBeAdded()
        {
            //GIVEN
            var actorDto = Any.Instance<ActorDto>();
            var requestFactory = NewInboundUpdateRequestFactory();
            var moviesController = new MoviesUpdateController(requestFactory);

            //WHEN
            var response = await moviesController.AddActor(actorDto);

            //THEN
            StatusCodeFrom(StatusCodeResultFrom(response)).Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async void ShouldReceivedResponseAboutUpdatedActor()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var actorDto = Any.Instance<ActorDto>();
            var requestFactory = NewInboundUpdateRequestFactory(movieRepository);
            var moviesController = new MoviesUpdateController(requestFactory);

            actorDto.Filmography.ForEach(movie =>
                movieRepository.GetById(movie.Id).Returns(ExistingMovie(movie.Id, Any.String())));

            //WHEN
            var response = await moviesController.UpdateActor(actorDto);

            //THEN
            StatusCodeFrom(JsonResultFrom(response)).Should().Be(StatusCodes.Status200OK);
            ModelFrom<ActorDto>(response).Should().Be(actorDto);
        }

        [Fact]
        public async void ShouldReceivedBadRequestWhenActorCouldNotBeUpdated()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var actorDto = Any.Instance<ActorDto>();
            var requestFactory = NewInboundUpdateRequestFactory(movieRepository);
            var moviesController = new MoviesUpdateController(requestFactory);

            actorDto.Filmography.ForEach(movie => movieRepository.GetById(movie.Id).Returns(NotExistingMovie()));

            //WHEN
            var response = await moviesController.UpdateActor(actorDto);

            //THEN
            StatusCodeFrom(StatusCodeResultFrom(response)).Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async void ShouldReceivedResponseAboutSuccessfullyLinkingExistingActorToExistingMovie()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var actorRepository = Substitute.For<IActorRepository>();
            var existingActorDto = Any.Instance<ExistingActorDto>();
            var movieId = Any.String();
            var actorId = Any.String();
            var requestFactory = NewInboundUpdateRequestFactory(actorRepository, movieRepository);
            var moviesController = new MoviesUpdateController(requestFactory);

            movieRepository.GetById(movieId).Returns(ExistingMovie(movieId, Any.String()));
            actorRepository.GetById(existingActorDto.Id).Returns(ExistingActor(actorId, Any.String()));

            //WHEN
            var response = await moviesController.LinkExistingActorToExistingMovie(existingActorDto, movieId);

            //THEN
            StatusCodeFrom(StatusCodeResultFrom(response)).Should().Be(StatusCodes.Status200OK);
        }

        [Fact]
        public async void ShouldReceivedNotModifiedForLinkingWhenActorNotExists()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var actorRepository = Substitute.For<IActorRepository>();
            var existingActorDto = Any.Instance<ExistingActorDto>();
            var movieId = Any.String();
            var requestFactory = NewInboundUpdateRequestFactory(actorRepository, movieRepository);
            var moviesController = new MoviesUpdateController(requestFactory);

            movieRepository.GetById(movieId).Returns(ExistingMovie(movieId, Any.String()));
            actorRepository.GetById(existingActorDto.Id).Returns(NotExistingActor());

            //WHEN
            var response = await moviesController.LinkExistingActorToExistingMovie(existingActorDto, movieId);

            //THEN
            StatusCodeFrom(StatusCodeResultFrom(response)).Should().Be(StatusCodes.Status304NotModified);
        }

        [Fact]
        public async void ShouldReceivedNotModifiedForLinkingWhenMovieNotExists()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var actorRepository = Substitute.For<IActorRepository>();
            var existingActorDto = Any.Instance<ExistingActorDto>();
            var movieId = Any.String();
            var requestFactory = NewInboundUpdateRequestFactory(actorRepository, movieRepository);
            var moviesController = new MoviesUpdateController(requestFactory);

            movieRepository.GetById(movieId).Returns(NotExistingMovie());
            actorRepository.GetById(existingActorDto.Id).Returns(ExistingActor(Any.String(), movieId));

            //WHEN
            var response = await moviesController.LinkExistingActorToExistingMovie(existingActorDto, movieId);

            //THEN
            StatusCodeFrom(StatusCodeResultFrom(response)).Should().Be(StatusCodes.Status304NotModified);
        }

        [Fact]
        public async void ShouldReceivedNotModifiedWhenExistingMovieHasAlreadyExistingActor()
        {
            //GIVEN
            var movieRepository = Substitute.For<IMovieRepository>();
            var actorRepository = Substitute.For<IActorRepository>();
            var existingActorDto = Any.Instance<ExistingActorDto>();
            var movieId = Any.String();
            var actorId = Any.String();
            var requestFactory = NewInboundUpdateRequestFactory(actorRepository, movieRepository);
            var moviesController = new MoviesUpdateController(requestFactory);

            movieRepository.GetById(movieId).Returns(ExistingMovie(movieId, actorId));
            actorRepository.GetById(existingActorDto.Id).Returns(ExistingActor(actorId, movieId));

            //WHEN
            var response = await moviesController.LinkExistingActorToExistingMovie(existingActorDto, movieId);

            //THEN
            StatusCodeFrom(StatusCodeResultFrom(response)).Should().Be(StatusCodes.Status304NotModified);
        }

        [Fact]
        public async void ShouldAlwaysReceivedOkWhenRemoveActor()
        {
            //GIVEN
            var requestFactory = NewInboundUpdateRequestFactory();
            var moviesController = new MoviesUpdateController(requestFactory);

            //WHEN
            var response = await moviesController.RemoveActor(Any.String());

            //THEN
            StatusCodeFrom(StatusCodeResultFrom(response)).Should().Be(StatusCodes.Status200OK);
        }

        [Fact]
        public async void ShouldAlwaysReceivedOkWhenRemoveMovie()
        {
            //GIVEN
            var requestFactory = NewInboundUpdateRequestFactory();
            var moviesController = new MoviesUpdateController(requestFactory);

            //WHEN
            var response = await moviesController.RemoveMovie(Any.String());

            //THEN
            StatusCodeFrom(StatusCodeResultFrom(response)).Should().Be(StatusCodes.Status200OK);
        }
    }
}