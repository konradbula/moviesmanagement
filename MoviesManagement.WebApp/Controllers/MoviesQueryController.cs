﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoviesManagement.Inbound.Adapters;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.WebApp.Controllers
{
    [Route("/")]
    [ApiController]
    public class MoviesQueryController : ControllerBase
    {
        private readonly IQueryRequestFactory _requestFactory;

        public MoviesQueryController(IQueryRequestFactory requestFactory)
        {
            _requestFactory = requestFactory;
        }

        [HttpGet("actors/all")]
        public async Task<IActionResult> GetAllActors()
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateAllActors(result));
        }

        [HttpGet("movies/all")]
        public async Task<IActionResult> GetAllMovies()
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateAllMovies(result));
        }

        [HttpGet("movies/actor/{actorId}")]
        public async Task<IActionResult> GetMoviesByActor(string actorId)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateMoviesByActor(actorId, result));
        }

        [HttpGet("movies/year/{year}")]
        public async Task<IActionResult> GetMoviesByYear(int year)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateMoviesByYear(year, result));
        }

        [HttpGet("actors/movie/{movieId}")]
        public async Task<IActionResult> GetActorsStarringInMovie(string movieId)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateActorsStarringInMovie(movieId, result));
        }

        private static async Task<IActionResult> ExecuteAsync(Func<IOperationResult, IInboundRequest> inboundRequest)
        {   
            var restActionResult = RestOperationResult.Create();
            await inboundRequest(restActionResult).Handle();
            return restActionResult.Response;
        }
    }
}