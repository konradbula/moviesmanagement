﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoviesManagement.Inbound.Adapters;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.WebApp.Controllers
{
    [Route("/")]
    [ApiController]
    public class MoviesUpdateController : ControllerBase
    {
        private readonly IUpdateRequestFactory _requestFactory;

        public MoviesUpdateController(IUpdateRequestFactory requestFactory)
        {
            _requestFactory = requestFactory;
        }

        [HttpPost("actor/add")]
        public async Task<IActionResult> AddActor([FromBody] ActorDto actorDto)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateAddActor(actorDto, result));
        }
        
        [HttpPut("actor/update")]
        public async Task<IActionResult> UpdateActor([FromBody] ActorDto actorDto)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateUpdateActor(actorDto, result));
        }
        
        [HttpDelete("actor/remove/{actorId}")]
        public async Task<IActionResult> RemoveActor(string actorId)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateRemoveActor(actorId, result));
        }
        
        [HttpPost("movie/add")]
        public async Task<IActionResult> AddMovie([FromBody] MovieDto movieDto)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateAddMovie(movieDto, result));
        }
        
        [HttpPut("movie/update")]
        public async Task<IActionResult> UpdateMovie([FromBody] MovieDto movieDto)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateUpdateMovie(movieDto, result));
        }

        [HttpDelete("movie/remove/{movieId}")]
        public async Task<IActionResult> RemoveMovie(string movieId)
        {   
            return await ExecuteAsync(result =>
                _requestFactory.CreateRemoveMovie(movieId, result));
        }

        [HttpPut("actor/link/movie/{movieId}")]
        public async Task<IActionResult> LinkExistingActorToExistingMovie([FromBody] ExistingActorDto existingActorDto, string movieId)
        {
            return await ExecuteAsync(result =>
                _requestFactory.CreateLinkExistingActorToExistingMovie(existingActorDto.Id, movieId, result));
        }
        
        [HttpPut("movie/link/actor/{actorId}")]
        public async Task<IActionResult> LinkExistingMovieToExistingActor([FromBody] ExistingMovieDto existingMovieDto, string actorId)
        {
            return await ExecuteAsync(result =>
                _requestFactory.CreateLinkExistingMovieToExistingActor(existingMovieDto.Id, actorId, result));
        }

        private static async Task<IActionResult> ExecuteAsync(Func<IOperationResult, IInboundRequest> inboundRequest)
        {   
            var restActionResult = RestOperationResult.Create();
            await inboundRequest(restActionResult).Handle();
            return restActionResult.Response;
        }
    }
}