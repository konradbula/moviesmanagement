﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using MoviesManagement.WebApp.Bootstrap;
using MoviesManagement.WebApp.Bootstrap.Configuration;

namespace MoviesManagement.WebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {   
            WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .PreferHostingUrls(true)
                .UseKestrel(KestrelConfiguration.Apply)
                .ConfigureAppConfiguration(AppConfiguration.Apply)
                .ConfigureLogging(LoggingConfiguration.Apply)
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build()
                .Run();
        }
    }
}