using Microsoft.Extensions.DependencyInjection;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.WebApp.Bootstrap
{
    public class DependencyResolver
    {
        private readonly IServiceCollection _serviceCollection;

        public DependencyResolver(IServiceCollection serviceCollection)
        {
            _serviceCollection = serviceCollection;
        }

        public void Register(IUpdateRequestFactory requestFactory)
        {
            _serviceCollection.AddSingleton(requestFactory);
        }

        public void Register(IQueryRequestFactory requestFactory)
        {
            _serviceCollection.AddSingleton(requestFactory);
        }
    }
}