﻿using System;
using System.Security.Authentication;
using Microsoft.ApplicationInsights;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using MoviesManagement.Inbound.Adapters;
using MoviesManagement.Inbound.Adapters.Repositories;
using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Dto;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.WebApp.Bootstrap.Configuration;

namespace MoviesManagement.WebApp.Bootstrap
{
    public class ServiceLogicRoot
    {
        private const string ConnectionString = @"mongodb://dev-project:Z2jHcn7E8vUeMU37W1jGCz7h3CszQb6XR1S7MxeQGtsf02gQIU4mntdCedYHp3ljPcXzbbKke6gkQBkzCfVudQ==@dev-project.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
        private const string DatabaseName = "MoviesManagement";
        private readonly ILogger<ServiceLogicRoot> _logger;

        public ServiceLogicRoot(DependencyResolver dependencyResolver,
            ILoggerFactory loggerFactory,
            TelemetryClient telemetryClient)
        {
            _logger = loggerFactory.CreateLogger<ServiceLogicRoot>();
            
            var restRequestCounter = new RestRequestCounter(telemetryClient);
            var mongoDatabase = GetMongoDatabase();
            var mongoDbActorRepository = new MongoDbActorRepository(mongoDatabase);
            var mongoDbMovieRepository = new MongoDbMovieRepository(mongoDatabase);
            var applicationPort = CreateApplicationPort(restRequestCounter, mongoDbActorRepository, mongoDbMovieRepository);
            var applicationLogicRoot = new ApplicationLogicRoot(loggerFactory, applicationPort);

            dependencyResolver.Register(applicationLogicRoot.UpdateRequestFactory);
            dependencyResolver.Register(applicationLogicRoot.QueryRequestFactory);
        }

        public void Start()
        {
            _logger.LogInformation("Web Application has been started...");
        }

        public void Stop()
        {
            _logger.LogInformation("Web Application has been closed.");
        }

        private static ApplicationPort CreateApplicationPort(IRequestCounter requestCounter,
            IActorRepository actorsRepository,
            IMovieRepository movieRepository)
        {
            return new ApplicationPort(requestCounter, actorsRepository, movieRepository);
        }

        private static IMongoDatabase GetMongoDatabase()
        {
            return MongoClient().GetDatabase(DatabaseName);
        }

        private static MongoClient MongoClient()
        {
            return new MongoClient(Settings());
        }

        private static MongoClientSettings Settings()
        {
            var settings = MongoClientSettings.FromUrl(
                new MongoUrl(ConnectionString)
            );
            settings.SslSettings = new SslSettings {EnabledSslProtocols = SslProtocols.Tls12};
            return settings;
        }
    }
}