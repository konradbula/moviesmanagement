﻿using Microsoft.Extensions.Logging;
using MoviesManagement.Inbound.Application.Requests;
using MoviesManagement.Inbound.Ports.Repositories;
using MoviesManagement.Inbound.Ports.Requests;

namespace MoviesManagement.WebApp.Bootstrap
{
    public class ApplicationLogicRoot
    {
        public IUpdateRequestFactory UpdateRequestFactory { get; }
        public IQueryRequestFactory QueryRequestFactory { get; }

        public ApplicationLogicRoot(ILoggerFactory loggerFactory, ApplicationPort applicationPort)
        {
            UpdateRequestFactory = new InboundUpdateRequestFactory(loggerFactory,
                applicationPort.RequestCounter,
                applicationPort.ActorsRepository,
                applicationPort.MovieRepository);

            QueryRequestFactory = new InboundQueryRequestFactory(loggerFactory,
                applicationPort.RequestCounter,
                applicationPort.ActorsRepository,
                applicationPort.MovieRepository);
        }
    }
}