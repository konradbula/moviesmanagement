﻿using System.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace MoviesManagement.WebApp.Bootstrap
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddSingleton(new DependencyResolver(services))
                .AddSingleton<ServiceLogicRoot>()
                .AddMvc();

            ServiceLogicRootFrom(services).Start();

            Trace.AutoFlush = true;
        }

        public void Configure(IApplicationBuilder applicationBuilder, IApplicationLifetime applicationLifetime)
        {
            applicationLifetime.ApplicationStopping.Register(ServiceLogicRootFrom(applicationBuilder).Stop);
            applicationBuilder.UseMvc();
        }

        private static ServiceLogicRoot ServiceLogicRootFrom(IServiceCollection services)
        {
            return services
                .BuildServiceProvider()
                .GetService<ServiceLogicRoot>();
        }

        private static ServiceLogicRoot ServiceLogicRootFrom(IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder
                .ApplicationServices
                .GetService<ServiceLogicRoot>();
        }
    }
}