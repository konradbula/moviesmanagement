﻿using MoviesManagement.Inbound.Ports;
using MoviesManagement.Inbound.Ports.Repositories;

namespace MoviesManagement.WebApp.Bootstrap
{
    public class ApplicationPort
    {
        public IActorRepository ActorsRepository { get; }
        public IMovieRepository MovieRepository { get; }
        public IRequestCounter RequestCounter { get; }

        public ApplicationPort(IRequestCounter requestCounter, 
            IActorRepository actorsRepository,
            IMovieRepository movieRepository)
        {
            ActorsRepository = actorsRepository;
            MovieRepository = movieRepository;
            RequestCounter = requestCounter;
        }
    }
}