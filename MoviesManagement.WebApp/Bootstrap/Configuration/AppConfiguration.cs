using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace MoviesManagement.WebApp.Bootstrap.Configuration
{
    public static class AppConfiguration
    {
        public static void Apply(WebHostBuilderContext hostingContext, IConfigurationBuilder config)
        {
            config
                .AddJsonFile(AppSettingsJsonFor(hostingContext.HostingEnvironment), optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
        }

        private static string AppSettingsJsonFor(IHostingEnvironment hostingEnvironment)
        {
            return hostingEnvironment.IsDevelopment() ?
                $"appsettings.{hostingEnvironment.EnvironmentName}.json" :
                "appsettings.json";
        }
    }
}