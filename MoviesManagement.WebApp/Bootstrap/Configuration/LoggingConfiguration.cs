using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace MoviesManagement.WebApp.Bootstrap.Configuration
{
    public static class LoggingConfiguration
    {
        private const string LogsDirectory = "Logs";
        private const string LogFile = "WebApp.log";
        
        public static void Apply(WebHostBuilderContext hostingContext, ILoggingBuilder logging)
        {
            logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
            logging.AddTraceSource(new SourceSwitch("WebAppAppSwitch", "Information"), FileTraceListener());
            logging.AddConsole();
            logging.AddDebug();
        }

        private static TextWriterTraceListener FileTraceListener()
        {
            Directory.CreateDirectory(LogsDirectory);
            return new TextWriterTraceListener(File.Create($"{LogsDirectory}/{LogFile}"));
        }
    }
}