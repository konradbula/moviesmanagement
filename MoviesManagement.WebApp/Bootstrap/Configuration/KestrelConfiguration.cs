using System.Net;
using System.Security.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace MoviesManagement.WebApp.Bootstrap.Configuration
{
    public static class KestrelConfiguration
    {
        private const string ServerCertificatePath = "Properties/asp-net-core-web-app-server-cert.pfx";
        private const string ServerCertificatePrivateKeyPassword = "UEVIMCt5Kzhbe3BIaExsIzJu";

        public static void Apply(KestrelServerOptions options)
        {
            options.Listen(IPAddress.Any, 5000);
            options.Listen(IPAddress.Any, 5001, listenOptions => listenOptions.UseHttps(
                ServerCertificatePath,
                ServerCertificatePrivateKeyPassword,
                configure =>
                {
                    configure.SslProtocols = SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;
                    configure.CheckCertificateRevocation = false;
                }));
        }
    }
}